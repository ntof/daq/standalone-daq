# ChangeLog

Only major or noticable changes are listed here, this list is **not** exhaustive.
## v1.1.0
-   :sparkles: RawFileMerger UI integration
## v1.0.2
-   :bug: fix acquisition start sequence, wait for the acquisition thread to be ready
## v1.0.1
-   :white_check_mark: Add RunNumber parameter
-   :white_check_mark: Add TimeWindow parameter
-   :bug: Fix lower-limit calculus
-   :lipstick: Fancier file-sizes (Kib, Mib ...)
-   :lipstick: Renamed some parameters (according to old GUI)
-   :lipstick: Fix units on some parameters (timing, ...)
-   :memo: Add some documentation on Calibration Timing (will be enhance with SSVG)

## v1.0.0

-   First release :tada:
