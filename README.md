# Standalone-DAQ for n_ToF

Web-based application to control an n_ToF DAQ.

This application is deployed there: <https://ntof-daq.web.cern.ch/standalone>

A *beta* version is also available there: <https://ntof-daq.web.cern.ch/standalone/beta>

This application is developped according to our [guidelines](https://mro-dev.web.cern.ch/docs/drafts/current/en-smm-apc-web-guidelines.html).

# Documentation

The complete n_ToF architecture is described in [n_ToF Software Architecture](https://mro-dev.web.cern.ch/docs/drafts/current/ntof-software-architecture.html).

# Build

To build this application application (assuming that Node.js is installed on your machine):
```bash
# Run webpack and bundle things in /dist
npm run build

# Serve pages on port 8080
npm run serve
```

# Deploy

To deploy the example application, assuming that oc is configured and connected
on the proper project:
```bash
cd deploy

./deploy.sh
```
