'use strict';

const _ = require('lodash');
const { before, after } = require('mocha');
const { createHook } = require('async_hooks');
const { stackTraceFilter } = require('mocha/lib/utils');
const allResources = new Map();

try {
  var process = require('process'); /* eslint-disable-line global-require */
}
catch (e) { } /* eslint-disable-line no-empty */

// this will pull Mocha internals out of the stacks
const filterStack = stackTraceFilter();

var hook;

if (_.get(process, 'env.ASYNC_DUMP') === '1') {

  before(function() {
    hook = createHook({
      init(asyncId, type, triggerAsyncId) {
        allResources.set(asyncId, { type, triggerAsyncId, stack: (new Error()).stack });
      },
      destroy(asyncId) {
        allResources.delete(asyncId);
      },
      promiseResolve(asyncId) { allResources.delete(asyncId); }
    }).enable();
  });

  after(function() {
    if (global.gc) {global.gc();}
    hook.disable();
    if (!_.isEmpty(allResources)) {
      console.error('STUFF STILL IN THE EVENT LOOP:');
      allResources.forEach((value) => {
        console.error(`Type: ${value.stack[0]}${value.type}`);
        console.error(filterStack(value.stack));
        console.error('\n');
      });
    }
  });
}
