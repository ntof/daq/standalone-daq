//@flow
import './karma_index';
import _ from "lodash";
import { BaseLogger as logger } from '@cern/base-vue';
import { waitFor } from './utils';
import * as utilities from "../src/utilities";

import { afterEach, beforeEach, describe, it } from 'mocha';
import { expect } from 'chai';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';
import 'tempusdominus-bootstrap'; // Needed for dynamic import

import store from '../src/store';
import RFMergerRuns from '../src/components/RFMerger/RFMergerRuns';

/*::
declare var serverRequire: (string) => any
*/

describe('RFMergerRuns', function() {
  let wrapper/*: Vue$Wrapper */;
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');
      const { DnsServer } = serverRequire('@cern/dim');
      const { DisXmlNode } = serverRequire('@ntof/dim-xml');
      const { RFMergerStub } = serverRequire('@ntof/ntof-stubs');

      this.env = {};
      return utils.createApp(this.env)
      .then(() => {
        this.env.dns = new DnsServer(null, 0);
        return this.env.dns.listen();
      })
      .then(() => {
        this.env.node = new DisXmlNode(null, 0);
        this.env.rfm = new RFMergerStub(this.env.dns.url());
        this.env.rfm.register("MERGER", this.env.node);
        return this.env.node.register(this.env.dns.url());
      })
      .then(() => {
        this.env.rfm.run();
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          server: this.env.server,
          dns: this.env.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(function() {
    return server.run(function() {
      this.env.rfm.close();
      this.env.server.close();
      this.env.node.close();
      this.env.dns.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* $FlowIgnore */
      wrapper = null;
    }
  });

  it('not initialized state', function() {
    store.commit('queryChange', { dns: null });
    wrapper = mount(RFMergerRuns, { propsData: { inEdit: false } });
    expect(wrapper.findAllComponents({ name: 'BaseCollapsible' }).length).to.equal(0);
  });

  it('initialized state', async function() {
    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(RFMergerRuns, { propsData: { inEdit: false } });

    await waitFor(wrapper, () => !wrapper.vm.loading, 2000);
    expect(wrapper.findAllComponents({ name: 'BaseCollapsible' }).length).to.be.above(0);
    const evtJson = JSON.stringify(wrapper.emitted());
    expect(evtJson).to.not.include('current-run');
    expect(evtJson).to.not.include('900003');
  });

  it('initialized state (fromCurrentRun)', async function() {
    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(RFMergerRuns, { propsData: { inEdit: false, fromCurrentRun: true } });

    await waitFor(wrapper, () => !wrapper.vm.loading, 2000);
    expect(wrapper.findAllComponents({ name: 'BaseCollapsible' }).length).to.be.above(0);
    const evtJson = JSON.stringify(wrapper.emitted());
    expect(evtJson).to.include('current-run');
    expect(evtJson).to.include('900003');
  });

  it('test Submit and Archive on Tape action', async () => {
    store.commit('queryChange', { dns: env.dns });
    logger.clear();
    wrapper = mount(RFMergerRuns, { propsData: { inEdit: false, fromCurrentRun: true } });

    await waitFor(wrapper, () => !wrapper.vm.loading, 2000);
    // Enable edit mode
    wrapper.setProps({ inEdit: true });
    await waitFor(wrapper, () => wrapper.find('.datetimepicker-input').exists());
    // Now Experiment is empty. Let's try to Archive on tape and get the error.
    let buttons = wrapper.findAll('button');
    buttons.filter((b) => b.html().includes('Archive')).at(0).trigger('click');

    await waitFor(wrapper, () => logger.getErrors().length > 0, 2000);
    expect(_.first(logger.getErrors())).to.have.property('message')
    .that.contains('It is not possible to approve a run without the experiment name.');

    // Set the experiment name.
    logger.clear();
    const params = wrapper.findAllComponents({ name: 'BaseParamInput' });
    params.filter((m) => m.vm.title === 'Experiment').at(0).find('input').setValue("test");
    await wrapper.vm.$nextTick();
    buttons = wrapper.findAll('button');
    buttons.filter((b) => b.html().includes('Submit')).at(0).trigger('click');

    await waitFor(wrapper, () => wrapper.vm.rfmRuns[0].experiment === "test", 2000);
    expect(wrapper.vm.rfmRuns[0].approved).to.equal(false);
    // Let's approve the run and start it.
    buttons = wrapper.findAll('button');
    buttons.filter((b) => b.html().includes('Archive'))
    .at(0).trigger('click');
    await waitFor(wrapper, () => wrapper.vm.rfmRuns[0].rate > 0, 2000);
  });
});
