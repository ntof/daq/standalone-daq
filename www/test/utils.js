//@flow

import { createLocalVue as tuCreateLocalVue } from '@vue/test-utils';
import VueRouter from 'vue-router';

import { defaultTo, delay, has, toString } from 'lodash';
import q from 'q';

import BaseVue from "@cern/base-vue";
import VueUtils from '../src/VueUtils';
import Vuex from 'vuex';

export function waitFor(
  vm /*: Vue$Wrapper|Vue$Component */,
  cb /*: () => boolean|any */,
  timeout /*: ?number */) /*: q$Promise<any> */ {

  if (has(vm, 'vm')) {
    vm = vm.vm; /* probably a wrapper */
  }
  var def = q.defer();
  var err = new Error('timeout'); /* create this early to have stacktrace */
  var timer = delay(def.reject.bind(def, err),
    defaultTo(timeout, 1000));

  function next() {
    /* $FlowIgnore */
    vm.$nextTick(() => {
      if (!def.promise.isPending()) {
        return;
      }
      try {
        var ret = cb(); /* eslint-disable-line callback-return */
        if (ret) {
          clearTimeout(timer);
          def.resolve(ret);
        }
        else {
          delay(next, 200);
        }
      }
      catch (e) {
        clearTimeout(timer);
        def.reject(has(e, 'message') ? e : new Error(e));
      }
    });
  }

  delay(next, 200);
  return def.promise;
}

export function /*:: <T> */ waitForWrapper(
  vm /*: Vue$Wrapper|Vue$Component */,
  test /*: () => Vue$Wrapper|Vue$WrapperArray */,
  timeout /*: ?number */) /*: q$Promise<any> */ {

  return waitFor(vm, () => {
    const wrapper = test();
    return wrapper.exists() ? wrapper : false;
  }, timeout);
}

/**
 * @param  {Vue} vm
 * @param  {() => any} test
 * @param  {any} value
 * @param  {number} [timeout]
 * @return {void}
 */
export function waitForValue(vm, test, value, timeout) {
  var _val;
  return waitFor(vm, () => {
    _val = test();
    return _val === value;
  }, timeout)
  .catch((err) => {
    const msg = "Invalid result value: " + toString(_val) + " != " + value;
    if (err instanceof Error) {
      err.message = msg;
      throw err;
    }
    throw new Error(msg);
  });
}

export const TransitionStub = {
  template: `<div :is="tag"><slot></slot></div>`,
  props: { tag: { type: String, default: 'div' } }
};

export const stubs = {
  'transition-group': TransitionStub,
  'transition': TransitionStub
};

export function createLocalVue() {
  const local = tuCreateLocalVue();
  local.use(VueRouter);
  local.use(BaseVue);
  local.use(VueUtils);
  local.use(Vuex);
  return local;
}
