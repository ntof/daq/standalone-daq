//@flow
import './karma_index';
import { waitFor } from './utils';
import * as utilities from "../src/utilities";
import { BaseLogger as logger } from '@cern/base-vue';
import { Area } from '../src/Consts';

import { afterEach, beforeEach, describe, it } from 'mocha';
import { expect } from 'chai';
import _ from "lodash";
import { createWrapper, mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';
import VueRouter from 'vue-router';

import store from '../src/store';
import Sidebar from '../src/components/SidebarContent';

/*::
declare var serverRequire: (string) => any
*/

describe('Sidebar', function() {
  let wrapper/*: Vue$Wrapper */;
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');
      const { DnsServer } = serverRequire('@cern/dim');
      const { DisXmlNode } = serverRequire('@ntof/dim-xml');
      const { DaqStub } = serverRequire('@ntof/ntof-stubs');

      this.env = {};
      return utils.createApp(this.env)
      .then(() => {
        this.env.dns = new DnsServer(null, 0);
        return this.env.dns.listen();
      })
      .then(() => {
        this.env.node = new DisXmlNode(null, 0);
        this.env.daq = new DaqStub(this.env.dns.url());
        this.env.daq.register("mydaq", this.env.node);
        return this.env.node.register(this.env.dns.url());
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          server: this.env.server,
          dns: this.env.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(function() {
    return server.run(function() {
      this.env.daq.close();
      this.env.server.close();
      this.env.node.close();
      this.env.dns.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* $FlowIgnore */
      wrapper = null;
    }
  });


  it('Lists default DNS', async () => {
    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(Sidebar);

    await waitFor(wrapper, () => wrapper.vm.daqList.length !== 0);
    expect(wrapper.find('.dropdown-menu').html()).to.contains('EAR1').and.contains('EAR2');

  });

  it('can list DAQs', async () => {
    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(Sidebar);

    await waitFor(wrapper, () => wrapper.vm.daqList.length !== 0);
    expect(wrapper.vm.daqList[0]).to.equal('mydaq');
  });

  it('reports an error if dns doesn\'t exist', async function() {
    store.commit('queryChange', { dns: 'this is not the dns you are looking for' });
    this.timeout(10000);
    logger.clear();
    wrapper = mount(Sidebar);

    await waitFor(wrapper, () => logger.getErrors().length !== 0, 5000);
    expect(_.first(logger.getErrors())).to.have.property('message')
    .that.contains('disconnected');
  });

  it('populates DAQs list when a DNS is selected', async () => {
    Area.EAR2.dns = env.dns;
    const router = new VueRouter({
      routes: [ {
        path: '/', name: 'Sidebar', component: Sidebar
      } ]
    });
    /* emulate our router */
    router.afterEach((to) => { /* jshint unused:false */
      store.commit('queryChange', to.query);
    });
    wrapper = mount({ template: '<router-view />' }, { router });

    await waitFor(wrapper, () => wrapper.find('.dropdown-menu'));
    const sidebar = createWrapper(_.get(wrapper.vm, [ '$children', 0 ]));
    expect(sidebar.find('.dropdown-menu').html())
    .to.contains('EAR1').and.contains('EAR2').and.contains('LAB');

    const dns = sidebar.findAll('.dropdown-item')
    .filter((m) => m.text() === 'EAR2');
    dns.trigger('click');

    await waitFor(wrapper, () => !_.isEmpty(router.currentRoute.query));
    await waitFor(sidebar, () => !_.isEmpty(sidebar.vm.daqList), 2000);
    expect(sidebar.find('.list-group').html()).to.contains('mydaq');

    const daq = sidebar.findAll('.list-group-item')
    .filter((p) => p.text() === 'mydaq');
    daq.trigger('click');

    if (sidebar) {
      sidebar.destroy();
    }
  });
});
