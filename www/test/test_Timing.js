//@flow
import './karma_index';

import { waitFor } from './utils';
import * as utilities from "../src/utilities";
import { BaseLogger as logger } from '@cern/base-vue';

import { afterEach, beforeEach, describe, it } from 'mocha';
import { expect } from 'chai';
import { first, startsWith } from 'lodash';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';

import store from '../src/store';
import Timing from '../src/components/Timing';

/*::
declare var serverRequire: (string) => any
*/


describe('Timing', function() {
  let wrapper/*: Vue$Wrapper */;
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');
      const { DnsServer } = serverRequire('@cern/dim');
      const { DisXmlNode } = serverRequire('@ntof/dim-xml');
      const { TimingStub } = serverRequire('@ntof/ntof-stubs');

      this.env = {};
      return utils.createApp(this.env)
      .then(() => {
        this.env.dns = new DnsServer(null, 0);
        return this.env.dns.listen();
      })
      .then(() => {
        this.env.node = new DisXmlNode(null, 0);
        this.env.stub = new TimingStub();
        this.env.stub.register(this.env.node);
        return this.env.node.register(this.env.dns.url());
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          server: this.env.server,
          dns: this.env.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(function() {
    return server.run(function() {
      this.env.stub.close();
      this.env.server.close();
      this.env.node.close();
      this.env.dns.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* $FlowIgnore */
      wrapper = null;
    }
  });

  it('displays timing information', async function() {
    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(Timing);

    await waitFor(wrapper, () => wrapper.vm.currentMode !== null);
    const mode = wrapper.vm.getModeName(wrapper.vm.currentMode);
    expect(mode).to.equal('DISABLED');
    expect(wrapper.vm.triggerPeriod).to.equal(2400);
    expect(wrapper.vm.triggerRepeat).to.equal(1);
    expect(wrapper.vm.triggerPause).to.equal(0);
    expect(wrapper.vm.eventNumber).to.equal(0);

    /* update the stub values */
    await server.run(function() {
      this.env.stub.update(1, 2); // CALIBRATION
      this.env.stub.update(2, 2);
      this.env.stub.update(3, 5000);
      this.env.stub.update(4, 1);
      this.env.stub.update(5, 10);
    });
    /* ensure updates are being displayed on front-end */
    await waitFor(wrapper, () => {
      const mode = wrapper.vm.getModeName(wrapper.vm.currentMode);
      return (mode === 'CALIBRATION') &&
        (wrapper.vm.triggerPeriod === 5000) &&
        (wrapper.vm.triggerRepeat === 2) &&
        (wrapper.vm.triggerPause === 1) &&
        (wrapper.vm.eventNumber === 10);
    });
  });

  it('displays an error when service crashes', async function() {
    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(Timing);

    await waitFor(wrapper, () => wrapper.vm.currentMode !== null);
    await server.run(function() {
      this.env.node.close();
    });
    await waitFor(wrapper, () => wrapper.vm.currentMode === null);
    expect(wrapper.find('.alert h6').text())
    .to.contains('Waiting for service');

    /* reconnect */
    await server.run(function() {
      const { DisXmlNode } = serverRequire('@ntof/dim-xml');
      this.env.node = new DisXmlNode(null, 0);
      this.env.node.addXmlParams('Timing', this.env.stub);
      return this.env.node.register(this.env.dns.url()).thenResolve(undefined);
    });
    await waitFor(wrapper, () => wrapper.vm.currentMode !== null);
  });

  it('reports an error if dns doesn\'t exist', async function() {
    store.commit('queryChange', { dns: 'this is not the dns you are looking for' });
    this.timeout(10000);
    logger.clear();
    wrapper = mount(Timing);

    await waitFor(wrapper, () => logger.getErrors().length > 0, 5000);
    expect(first(logger.getErrors())).to.have.property('message')
    .that.contains('disconnected');
    await waitFor(wrapper, () => wrapper.vm.currentMode === null);
  });

  it('reports an error if server crashes', async function() {
    store.commit('queryChange', { dns: env.dns });
    logger.clear();
    await server.run(function() {
      this.env.server.close();
    });
    wrapper = mount(Timing);

    await waitFor(wrapper, () => logger.getErrors().length > 0);
    expect(first(logger.getErrors())).to.have.property('message')
    .that.contains('NetworkError');
    await waitFor(wrapper, () => wrapper.vm.currentMode === null);
  });

  it('can reconnect to service', async function() {
    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(Timing);

    await waitFor(wrapper, () => wrapper.vm.currentMode !== null);
    const mode = wrapper.vm.getModeName(wrapper.vm.currentMode);
    expect(mode).to.equal('DISABLED');
    expect(wrapper.vm.triggerPeriod).to.equal(2400);
    expect(wrapper.vm.triggerRepeat).to.equal(1);
    expect(wrapper.vm.triggerPause).to.equal(0);
    expect(wrapper.vm.eventNumber).to.equal(0);

    await server.run(function() {
      this.env.node.close();
    });
    await waitFor(wrapper, () => wrapper.vm.currentMode === null);
    expect(wrapper.find('.alert h6').text())
    .to.contains('Waiting for service');

    /* reconnect */
    await server.run(function() {
      const { DisXmlNode } = serverRequire('@ntof/dim-xml');
      this.env.node = new DisXmlNode(null, 0);
      this.env.node.addXmlParams('Timing', this.env.stub);
      return this.env.node.register(this.env.dns.url()).thenResolve(undefined);
    });
    await waitFor(wrapper, () => wrapper.vm.currentMode !== null);

    /* update the stub values */
    await server.run(function() {
      this.env.stub.update(1, 2); // CALIBRATION
      this.env.stub.update(2, 2);
      this.env.stub.update(3, 5000);
      this.env.stub.update(4, 1);
      this.env.stub.update(5, 10);
    });
    /* ensure updates are being displayed on front-end */
    await waitFor(wrapper, () => {
      const mode = wrapper.vm.getModeName(wrapper.vm.currentMode);
      return (mode === 'CALIBRATION') &&
          (wrapper.vm.triggerPeriod === 5000) &&
          (wrapper.vm.triggerRepeat === 2) &&
          (wrapper.vm.triggerPause === 1) &&
          (wrapper.vm.eventNumber === 10);
    });
  });

  it('can edit configuration', async () => {
    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(Timing);

    /* wait for component to setup */
    await waitFor(wrapper, () => wrapper.vm.currentMode !== null);
    wrapper.find('.fa-cog').trigger('click');
    await waitFor(wrapper, () => wrapper.vm.inEdit);

    /* set editMode to CALIBRATION */
    const calib = wrapper.findAll('.dropdown-item')
    .filter((m) => m.text() === 'CALIBRATION');
    calib.trigger('click');

    await waitFor(wrapper,
      () => wrapper.vm.getModeName(wrapper.vm.editMode) === 'CALIBRATION');
    /* configure CALIBRATION mode */

    const params = wrapper.findAll('.b-param');
    params.filter((p) => startsWith(p.vm.title, 'Period'))
    .at(0).find('input').setValue('500');

    params.filter((p) => p.vm.title === 'Repeat')
    .at(0).find('input').setValue('2');

    params.filter((p) => startsWith(p.vm.title, 'Pause'))
    .at(0).find('input').setValue('1');

    /* push new config */
    wrapper.findAll('button')
    .filter((b) => b.text() === 'Submit')
    .trigger('click');

    /* check that config is applied */
    await waitFor(wrapper, () => {
      const mode = wrapper.vm.getModeName(wrapper.vm.currentMode);
      return !wrapper.vm.inEdit && (mode === 'CALIBRATION') &&
        (wrapper.vm.triggerPeriod === 500) &&
        (wrapper.vm.triggerRepeat === 2) &&
        (wrapper.vm.triggerPause === 1);
    });
  });
});
