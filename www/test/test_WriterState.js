//@flow
import './karma_index';
import { waitFor } from './utils';
import * as utilities from "../src/utilities";
import { BaseLogger as logger } from '@cern/base-vue';

import { afterEach, beforeEach, describe, it } from 'mocha';
import { expect } from 'chai';
import _ from 'lodash';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';

import store from '../src/store';
import Writer from '../src/components/Daq';

/*::
declare var serverRequire: (string) => any
*/

describe('WriterState', function() {
  let wrapper/*: Vue$Wrapper */;
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');
      const { DnsServer } = serverRequire('@cern/dim');
      const { DisXmlNode } = serverRequire('@ntof/dim-xml');
      const { DaqStub } = serverRequire('@ntof/ntof-stubs');

      this.env = {};
      return utils.createApp(this.env)
      .then(() => {
        this.env.dns = new DnsServer(null, 0);
        return this.env.dns.listen();
      })
      .then(() => {
        this.env.node = new DisXmlNode(null, 0);
        this.env.stub = new DaqStub();
        this.env.stub.register('mydaq', this.env.node);
        return this.env.node.register(this.env.dns.url());
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          server: this.env.server,
          dns: this.env.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(function() {
    return server.run(function() {
      this.env.server.close();
      this.env.node.close();
      this.env.dns.close();
      this.env.stub.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* $FlowIgnore */
      wrapper = null;
    }
  });


  it('displays writer state information', async function() {
    store.commit('queryChange', { dns: env.dns, daq: 'mydaq' });
    wrapper = mount(Writer, { propsData: { canConfigure: env.canConfigure } });

    await waitFor(wrapper, () => wrapper.vm.$refs.writerState.state !== null);
    expect(wrapper.vm.$refs.writerState.strState).to.equal('Idle');
    expect(wrapper.vm.$refs.writerState.state).to.equal(0);

    /* update the stub values */
    await server.run(function() {
      this.env.stub.writerState.setState(1);
    });

    /* ensure updates are being displayed on front-end */
    await waitFor(wrapper, () => (wrapper.vm.$refs.writerState.state === 1) && (wrapper.vm.$refs.writerState.strState === 'Initialization'));
  });

  it('displays an error when service crashes', async function() {
    store.commit('queryChange', { dns: env.dns, daq: 'mydaq' });
    wrapper = mount(Writer, { propsData: { canConfigure: env.canConfigure } });

    await waitFor(wrapper, () => !_.isNil(wrapper.vm.$refs.writerState.state));
    await server.run(function() {
      this.env.node.close();
    });
    await waitFor(wrapper, () => _.isNil(wrapper.vm.$refs.writerState.state));
    expect(wrapper.find('.alert h6').text())
    .to.contains('Waiting for service');

    /* reconnect */
    await server.run(function() {
      const { DisXmlNode } = serverRequire('@ntof/dim-xml');
      this.env.node = new DisXmlNode(null, 0);
      this.env.stub.register('mydaq', this.env.node);
      return this.env.node.register(this.env.dns.url()).thenResolve(undefined);
    });
    await waitFor(wrapper,
      () => waitFor(wrapper, () => !_.isNil(wrapper.vm.$refs.writerState.state)));
  });

  it('reports an error if dns doesn\'t exist', async function() {
    store.commit('queryChange', { dns: 'this is not the dns you are looking for', daq: 'mydaq' });
    this.timeout(10000);
    logger.clear();
    wrapper = mount(Writer, {
      propsData: { canConfigure: env.canConfigure }
    });

    await waitFor(wrapper, () => logger.getErrors().length > 0, 5000);
    expect(_.first(logger.getErrors())).to.have.property('message')
    .that.contains('disconnected');
    await waitFor(wrapper, () => wrapper.vm.$refs.writerState.state === undefined);
  });

  it('reports an error if server crashes', async function() {
    store.commit('queryChange', { dns: env.dns, daq: 'mydaq' });
    logger.clear();
    await server.run(function() {
      this.env.server.close();
    });
    wrapper = mount(Writer, { propsData: { canConfigure: env.canConfigure } });

    await waitFor(wrapper, () => logger.getErrors().length > 0);
    expect(_.first(logger.getErrors())).to.have.property('message')
    .that.contains('NetworkError');
    await waitFor(wrapper, () => wrapper.vm.$refs.writerState.state === undefined);
  });

  it('can reconnect to service', async function() {
    store.commit('queryChange', { dns: env.dns, daq: 'mydaq' });
    wrapper = mount(Writer, { propsData: { canConfigure: env.canConfigure } });

    await waitFor(wrapper, () => wrapper.vm.$refs.writerState.state !== null);
    expect(wrapper.vm.$refs.writerState.strState).to.equal('Idle');
    expect(wrapper.vm.$refs.writerState.state).to.equal(0);
    await server.run(function() {
      this.env.node.close();
    });
    await waitFor(wrapper, () => _.isNil(wrapper.vm.$refs.writerState.state));
    expect(wrapper.find('.alert h6').text())
    .to.contains('Waiting for service'); /* Currently displays "Please select a DAQ...", to be changed in the correspondent component */

    /* reconnect */
    await server.run(function() {
      const { DisXmlNode } = serverRequire('@ntof/dim-xml');
      this.env.node = new DisXmlNode(null, 0);
      this.env.stub.register('mydaq', this.env.node);
      return this.env.node.register(this.env.dns.url()).thenResolve(undefined);
    });
    await waitFor(wrapper, () => !_.isNil(wrapper.vm.$refs.writerState.state));

    /* update the stub values */
    await server.run(function() {
      this.env.stub.writerState.setState(1);
    });
    /* ensure updates are being displayed on front-end */
    await waitFor(wrapper,
      () => (wrapper.vm.$refs.writerState.state === 1) && (wrapper.vm.$refs.writerState.strState === 'Initialization'));
  });
});
