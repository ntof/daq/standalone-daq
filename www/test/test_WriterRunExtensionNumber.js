//@flow
import './karma_index';
import { waitFor } from './utils';
import * as utilities from "../src/utilities";
import { BaseLogger as logger } from '@cern/base-vue';

import { afterEach, beforeEach, describe, it } from 'mocha';
import { expect } from 'chai';
import _ from "lodash";
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';

import store from '../src/store';
import Writer from '../src/components/Daq';

/*::
declare var serverRequire: (string) => any
*/

describe('WriterRun', function() {
  let wrapper/*: Vue$Wrapper */;
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');
      const { DnsServer, DisService, DisNode } = serverRequire('@cern/dim');

      this.env = {};
      return utils.createApp(this.env)
      .then(() => {
        this.env.dns = new DnsServer(null, 0);
        return this.env.dns.listen();
      })
      .then(() => {
        this.env.service = new DisService('C', '<RunExtensionNumberWrote run="-1" event="-1" size="0"/>');
        this.env.node = new DisNode(null, 0);
        this.env.node.addService('mydaq/WRITER/RunExtensionNumber', this.env.service);
        return this.env.node.register(this.env.dns.url());
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          server: this.env.server,
          dns: this.env.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(function() {
    return server.run(function() {
      this.env.server.close();
      this.env.node.close();
      this.env.dns.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* $FlowIgnore */
      wrapper = null;
    }
  });

  it('displays writer run extension number information', async function() {
    store.commit('queryChange', { dns: env.dns, daq: 'mydaq' });
    wrapper = mount(Writer);

    await waitFor(wrapper, () => wrapper.vm.runNumber !== null && wrapper.vm.eventNumber !== null);
    expect(wrapper.vm.runNumber).to.equal(-1);
    expect(wrapper.vm.eventNumber).to.equal(-1);
  });

  it('reports an error if dns doesn\'t exist', async function() {
    store.commit('queryChange', { dns: 'this is not the dns you are looking for', daq: 'mydaq' });
    this.timeout(10000);
    logger.clear();
    wrapper = mount(Writer);

    await waitFor(wrapper, () => logger.getErrors().length > 0, 5000);
    expect(_.first(logger.getErrors())).to.have.property('message')
    .that.contains('disconnected');
    await waitFor(wrapper, () =>  wrapper.vm.runNumber === null && wrapper.vm.eventNumber === null);
  });

  it('reports an error if server crashes', async function() {
    store.commit('queryChange', { dns: env.dns, daq: 'mydaq' });
    logger.clear();

    await server.run(function() {
      this.env.server.close();
    });
    wrapper = mount(Writer);

    await waitFor(wrapper, () => logger.getErrors().length > 0);
    expect(_.first(logger.getErrors())).to.have.property('message')
    .that.contains('NetworkError');
    await waitFor(wrapper, () => wrapper.vm.runNumber === null && wrapper.vm.eventNumber === null);
  });

  it('can reconnect to service', async function() {
    store.commit('queryChange', { dns: env.dns, daq: 'mydaq' });
    wrapper = mount(Writer);

    await waitFor(wrapper, () => wrapper.vm.runNumber !== null && wrapper.vm.eventNumber !== null);
    expect(wrapper.vm.runNumber).to.equal(-1);
    expect(wrapper.vm.eventNumber).to.equal(-1);
    await server.run(function() {
      this.env.node.close();
    });
    await waitFor(wrapper, () => wrapper.vm.runNumber === null && wrapper.vm.eventNumber === null);
    expect(wrapper.find('.alert h6').text())
    .to.contains('Waiting for service');

    /* reconnect */
    await server.run(function() {
      const { DisService, DisNode } = serverRequire('@cern/dim');
      this.env.service = new DisService('C', '<RunExtensionNumberWrote run="-1" event="-1" size="0"/>');
      this.env.node = new DisNode(null, 0);
      this.env.node.addService('mydaq/WRITER/RunExtensionNumber', this.env.service);
      return this.env.node.register(this.env.dns.url()).thenResolve(undefined);
    });
    await waitFor(wrapper, () => wrapper.vm.runNumber !== null && wrapper.vm.eventNumber !== null);
  });
});
