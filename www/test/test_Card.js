//@flow
import './karma_index';

import { afterEach, beforeEach, describe, it } from 'mocha';
import { expect } from "chai";
import _ from 'lodash';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';

import { waitFor, waitForValue, waitForWrapper } from './utils';
import * as utilities from "../src/utilities";

import store from '../src/store';
import CardList from '../src/components/CardList';

/*::
declare var serverRequire: (string) => any
*/

describe('Card', function() {
  let wrapper/*: Vue$Wrapper */;
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');
      const { DnsServer } = serverRequire('@cern/dim');
      const { DisXmlNode } = serverRequire('@ntof/dim-xml');
      const { DaqStub } = serverRequire('@ntof/ntof-stubs');

      this.env = {};
      return utils.createApp(this.env)
      .then(() => {
        this.env.dns = new DnsServer(null, 0);
        return this.env.dns.listen();
      })
      .then(() => {
        this.env.node = new DisXmlNode(null, 0);
        this.env.daq = new DaqStub(this.env.dns.url());
        this.env.daq.run();
        this.env.daq.register("mydaq", this.env.node);
        return this.env.node.register(this.env.dns.url());
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          server: this.env.server,
          dns: this.env.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(function() {
    return server.run(function() {
      this.env.daq.close();
      this.env.server.close();
      this.env.node.close();
      this.env.dns.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* $FlowIgnore */
      wrapper = null;
    }
  });

  it('displays card information', async function() {
    store.commit('queryChange', { dns: env.dns, daq: 'mydaq' });
    wrapper = mount(CardList);

    await waitFor(wrapper, () => wrapper.vm.cards !== null);
    await waitFor(wrapper, () => !wrapper.vm.isLoading);
    const cards = wrapper.vm.$refs['Cards'];
    expect(cards).to.not.be.empty();
    _.forEach(cards, (card) => {
      expect(card.sampleRate).to.equal(1800);
      expect(card.sampleSize).to.equal(128000);
      expect(card.delay).to.equal(0);
      const channels =  card.$refs['Channels'];
      _.forEach(channels, (channel) => {
        expect(channel.detectorType).to.equal('BAF2');
      }
      );
    });
    /* update the stub values */
    await server.run(function() {
      this.env.daq.cards[0].channels[0].update(2, 'BAF3');
    });
    /* ensure updates are being displayed on front-end */
    await waitFor(wrapper, () => wrapper.vm.$refs['Cards'][0].$refs['Channels'][0].detectorType === 'BAF3');
  });

  it('updates sampleSize if timeWindow is modified', async function() {
    store.commit('queryChange', { dns: env.dns, daq: 'mydaq' });
    wrapper = mount(CardList);

    await waitFor(wrapper, () => wrapper.vm.cards !== null);
    await waitFor(wrapper, () => !wrapper.vm.isLoading);
    const card = await waitForWrapper(wrapper, () => wrapper.findComponent({ ref: 'Cards' }));
    expect(card.findComponent({ ref: 'sampleSize' }).find('input'))
    .to.have.nested.property('element.value', '128000');

    /* enter edit mode */
    wrapper.find('.fa-cog').trigger('click');
    await waitFor(wrapper, () => wrapper.vm.inEdit);
    /* 100 ms window */
    card.findComponent({ ref: 'timeWindow' }).find('input').setValue('100');
    await wrapper.vm.$nextTick();
    /* sampleSize is updated when timeWindow is modified */
    expect(card.findComponent({ ref: 'sampleSize' }).find('input'))
    .to.have.nested.property('element.value', '180000');
  });

  it('updates sampleSize if SampleRate is modified', async function() {
    store.commit('queryChange', { dns: env.dns, daq: 'mydaq' });
    wrapper = mount(CardList);

    await waitFor(wrapper, () => wrapper.vm.cards !== null);
    await waitFor(wrapper, () => !wrapper.vm.isLoading);
    const card = await waitForWrapper(wrapper, () => wrapper.findComponent({ ref: 'Cards' }));
    expect(card.findComponent({ ref: 'sampleSize' }).find('input'))
    .to.have.nested.property('element.value', '128000');
    /* enter edit mode */
    wrapper.find('.fa-cog').trigger('click');
    await waitFor(wrapper, () => wrapper.vm.inEdit);
    await waitFor(wrapper, () => card.findComponent({ ref: 'sampleRate' }).find('select').exists());
    expect(card.findComponent({ ref: 'sampleRate' }).findAll('select > option').exists());
    card.findComponent({ ref: 'sampleRate' }).findAll('select > option').at(0).setSelected();
    await wrapper.vm.$nextTick();
    /* sampleSize is updated when SampleRate is modified */
    expect(card.findComponent({ ref: 'sampleSize' }).find('input'))
    .to.have.nested.property('element.value', '1000');
  });

  it('can edit configuration', async () => {
    store.commit('queryChange', { dns: env.dns, daq: 'mydaq' });
    wrapper = mount(CardList);

    /* wait for component to setup */
    await waitFor(wrapper, () => wrapper.vm.cards !== null);
    await waitFor(wrapper, () => !wrapper.vm.isLoading);
    await waitFor(wrapper, () => wrapper.vm.canConfigure);
    await wrapper.find('.fa-cog').trigger('click');
    await waitFor(wrapper, () => wrapper.vm.inEdit);

    const channel = _.get(wrapper.vm.$refs,
      [ 'Cards', 0, '$refs', 'Channels', 0 ]);
    const params = wrapper.findAll('.b-param');
    params.filter((m) => m.vm.title === 'Detector Name')
    .at(0).find('input').setValue('test');
    params.filter((m) => m.vm.title === 'Detector ID')
    .at(0).find('input').setValue('4242');

    // @edit signals are asynchronuous, let's wait to detect the changes
    await waitForValue(wrapper,
      () => _.get(channel, 'edit.detectorType'), 'test');

    /* push new config */
    wrapper.findAll('button')
    .filter((b) => b.text() === 'Configure')
    .trigger('click');
    await waitFor(wrapper, () => (!wrapper.vm.isLoading && !wrapper.vm.inEdit));
    const chan = _.get(wrapper.vm.$refs, [ 'Cards', 0, '$refs', 'Channels', 0 ]);

    expect(chan).to.have.property('detectorType', 'test');
    expect(chan).to.have.property('detectorId', 4242);
  });

  function testError(value, selector, title, isReadOnly = false) {
    it(`displays an alert for "${value}" in "${title}"`, async () => {
      store.commit('queryChange', { dns: env.dns, daq: 'mydaq' });
      wrapper = mount(CardList);

      /* wait for component to setup */
      await waitFor(wrapper, () => wrapper.vm.cards !== null);
      await waitFor(wrapper, () => !wrapper.vm.isLoading);
      wrapper.find('.fa-cog').trigger('click');
      await waitFor(wrapper, () => wrapper.vm.inEdit);
      const params = wrapper.findAll('.b-param');
      const param = params.filter((m) => m.vm.title === title).at(0);
      if (isReadOnly) { param.vm.$data.editValue = value; }
      else { param.find('input').setValue(value); }
      await waitFor(wrapper, () => wrapper.find(selector).exists());
    });
  }

  it(`displays an alert for Time Window [ms]`, async () => {
    store.commit('queryChange', { dns: env.dns, daq: 'mydaq' });
    wrapper = mount(CardList);

    /* wait for component to setup */
    await waitFor(wrapper, () => wrapper.vm.cards !== null);
    await waitFor(wrapper, () => !wrapper.vm.isLoading);
    wrapper.find('.fa-cog').trigger('click');

    await waitFor(wrapper, () => wrapper.vm.inEdit);
    expect(wrapper.find('.alert-danger').exists()).to.equal(false);
    let params = wrapper.findAll('.b-param');
    let computedValue = 175001 / wrapper.vm.$refs['Cards'][0].$refs['Channels'][0].sampleRate;
    params.filter((m) => m.vm.title === 'Time Window [ms]')
    .at(0).find('input').setValue(computedValue);

    await waitFor(wrapper, () => wrapper.find('.alert-danger').exists());
    params = wrapper.findAll('.b-param');
    computedValue = 175000 / wrapper.vm.$refs['Cards'][0].$refs['Channels'][0].sampleRate;
    params.filter((m) => m.vm.title === 'Time Window [ms]')
    .at(0).find('input').setValue(computedValue);

    await wrapper.vm.$nextTick();
    expect(!wrapper.find('.alert-warning').exists());
    params = wrapper.findAll('.b-param');
    computedValue = 0 / wrapper.vm.$refs['Cards'][0].$refs['Channels'][0].sampleRate;
    params.filter((m) => m.vm.title === 'Time Window [ms]')
    .at(0).find('input').setValue(computedValue);

    await waitFor(wrapper, () => wrapper.find('.alert-danger').exists());
  });

  testError(175001, '.alert-danger', 'Sample Size [KS]', true); /* Please select a value that is no more than 175000. */
  testError(0, '.alert-danger', 'Sample Size [KS]', true); /* Please select a value that is no less than 1. */
  testError(25001, '.alert-warning', 'Offset [mV]', true); /* offset should be below 2500 and above -2500 */
});
