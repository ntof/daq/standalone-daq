/* eslint-disable max-lines */
import './karma_index';
//@flow

import _ from "lodash";
import VueRouter from 'vue-router';
import { createLocalVue, waitFor, waitForWrapper } from './utils';
import * as utilities from "../src/utilities";

import { afterEach, beforeEach, describe, it } from 'mocha';
import { expect } from 'chai';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';

import { DicXmlValue } from '@ntof/redim-client';

import store from '../src/store';
import Dashboard from '../src/components/Dashboard';

/*::
declare var serverRequire: (string) => any
*/

describe('StandaloneDaq', function() {
  let wrapper/*: Vue$Wrapper */;
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');
      const { DnsServer } = serverRequire('@cern/dim');
      const { DisXmlNode } = serverRequire('@ntof/dim-xml');
      const { DaqStub, RFMergerStub, TimingStub } = serverRequire('@ntof/ntof-stubs');

      this.env = {};
      return utils.createApp(this.env)
      .then(() => {
        this.env.dns = new DnsServer(null, 0);
        return this.env.dns.listen();
      })
      .then(() => {
        this.env.node = new DisXmlNode(null, 0);
        this.env.rfm = new RFMergerStub(this.env.dns.url());
        this.env.rfm.register("MERGER", this.env.node);
        this.env.daq = new DaqStub(this.env.dns.url());
        this.env.timing = new TimingStub();
        this.env.timing.on("event", () => this.env.daq.trigger());
        this.env.daq.register("mydaq", this.env.node);
        this.env.timing.register(this.env.node);
        return this.env.node.register(this.env.dns.url());
      })
      .then(() => {
        this.env.daq.run();
        this.env.rfm.run();
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          dns: this.env.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(function() {
    return server.run(function() {
      this.env.rfm.close();
      this.env.timing.close();
      this.env.daq.close();
      this.env.server.close();
      this.env.node.close();
      this.env.dns.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* $FlowIgnore */
      wrapper = null;
    }
  });


  it('contains the correct options in url', async function()  {
    store.commit('queryChange', { dns: env.dns, daq: 'mydaq' });
    const router = new VueRouter({
      routes: [ {
        path: '/', name: 'Dashboard', component: Dashboard
      } ]
    });
    /* emulate our router */
    router.afterEach((to) => { /* jshint unused:false */
      store.commit('queryChange', to.query);
    });
    /* fix issue with vue-test-utils 31, router is not updated for some reasons */
    router.push = _.wrap(router.push, function(p, arg) {
      router.app.$nextTick(() => p.call(router, arg));
    });
    wrapper = mount({ template: '<router-view />' }, { router, localVue: createLocalVue() });

    router.push({ query: { dns: env.dns } });

    const timing = await waitForWrapper(wrapper, () => wrapper.findComponent({ name: 'Timing' }));
    await waitFor(timing, () => !_.isEmpty(timing.vm.dns));
    expect(timing.vm.dns).to.equal(env.dns);
    let sidebar = wrapper.findComponent({ name: 'SidebarContent' });
    await waitFor(sidebar, () => !_.isEmpty(sidebar.vm.daqList));

    sidebar = wrapper.findComponent({ name: 'SidebarContent' });
    const input = sidebar.findAll('.daq');

    expect(input.length).to.equal(1);
    expect(input.at(0).html()).to.contains("mydaq");
    input.at(0).trigger("click");

    /* ensure daq is connected */
    const daq = wrapper.findComponent({ name: 'Daq' });
    expect(daq.exists()).to.be.true();
    expect(daq.vm.dns).to.equal(env.dns);
    await waitFor(wrapper, () => !_.isEmpty(daq.vm.daq));

    /* ensure cardlist is connected */
    const cardlist = wrapper.findComponent({ name: 'CardList' });
    expect(cardlist.exists()).to.be.true();
    expect(cardlist.vm.dns).to.equal(env.dns);
  });

  it('can run an acquisition', async function() { // eslint-disable-line max-statements
    this.timeout(10000);
    const router = new VueRouter({
      routes: [ {
        path: '/', name: 'Dashboard', component: Dashboard
      } ]
    });
    /* emulate our router */
    router.afterEach((to) => { /* jshint unused:false */
      store.commit('queryChange', to.query);
    });
    /* fix issue with vue-test-utils 31, router is not updated for some reasons */
    router.push = _.wrap(router.push, function(p, arg) {
      router.app.$nextTick(() => p.call(router, arg));
    });
    wrapper = mount({ template: '<router-view />' }, { router, localVue: createLocalVue() });

    router.push({ query: { dns: env.dns } });

    const timing = await waitForWrapper(wrapper, () => wrapper.findComponent({ name: 'Timing' }));
    await waitFor(timing, () => !_.isEmpty(timing.vm.dns));
    /* ensure timing is connected */
    expect(timing.vm.dns).to.equal(env.dns);

    let button = await waitForWrapper(wrapper, () => timing.find('.fa-cog'));
    button.trigger('click');
    const option = await waitForWrapper(timing, () => {
      return timing.findAll('.dropdown-item')
      .filter((m) => m.text() === 'CALIBRATION');
    });
    /* set editMode to CALIBRATION */
    option.trigger('click');
    await waitForWrapper(timing, () => timing.findComponent({ ref: 'period' }));
    timing.findComponent({ ref: 'period' }).find('input').setValue('500');
    timing.findComponent({ ref: 'repeat' }).find('input').setValue('1');
    timing.findComponent({ ref: 'pause' }).find('input').setValue('0');

    wrapper.findAll('button').filter((b) => b.text() === 'Submit')
    .trigger('click');
    await waitFor(timing,
      () => timing.vm.getModeName(timing.vm.currentMode) === 'CALIBRATION');

    const sidebar = wrapper.findComponent({ name: 'SidebarContent' });

    const input = await waitForWrapper(wrapper, () => sidebar.findAll('.daq'));
    expect(input.length).to.equal(1);
    expect(input.at(0).html()).to.contains("mydaq");
    input.at(0).trigger("click");

    /* ensure cardlist is connected */
    var cardlist = wrapper.findComponent({ name: 'CardList' });
    expect(cardlist.exists()).to.be.true();
    expect(cardlist.vm.dns).to.equal(env.dns);

    const cog = await waitForWrapper(wrapper, () => cardlist.find('.fa-cog'));
    cog.trigger('click');
    await waitFor(wrapper, () => cardlist.vm.inEdit);
    const params = cardlist.findAll('.b-param');
    params.filter((m) => m.vm.title === 'Detector Name')
    .at(0).find('input').setValue('test');

    /* push new config */
    button = await waitForWrapper(wrapper, () => {
      return cardlist.findAll('button:not(disabled)')
      .filter((b) => b.text() === 'Configure');
    });
    button.trigger('click');
    await waitFor(cardlist, () => waitFor(cardlist, () => !cardlist.vm.isLoading));
    await waitFor(cardlist, () => cardlist.vm.$refs['Cards'][0].$refs['Channels'][0].detectorType === 'test');

    const client = new DicXmlValue('mydaq/WRITER/RunExtensionNumber',
      { proxy: env.proxyUrl }, utilities.dnsUrl(env.dns));
    let values = [];
    client.on('value', (value) => {
      values.push(_.toNumber(_.invoke(value, 'getAttribute', 'event')));
    });

    /* ensure daq is connected */
    const daq = wrapper.findComponent({ name: 'Daq' });
    expect(daq.exists()).to.be.true();
    expect(daq.vm.dns).to.equal(env.dns);

    await waitFor(wrapper, () => !_.isNil(daq.vm.daqState));
    daq.find('.fa-cog').trigger('click');
    const runNum = await waitForWrapper(wrapper, () => wrapper.findComponent({ name: 'AppRunNumberParam' }));
    runNum.find('input').setValue('1234');
    button = await waitForWrapper(wrapper, () => {
      return wrapper.findAll('button:not(disabled)')
      .filter((b) => b.text() === 'Start');
    });
    button.trigger('click');
    await waitFor(wrapper, () => daq.vm.isRunning);
    /* only modified after first acquisition */
    await waitFor(wrapper, () => daq.vm.runNumber === 1234, 2500);
    await client.promise();
    values = _.filter(values, (v) => v >= 0);
    /* ensure that the first recorded event is numbered 1 */
    expect(values).to.have.property('0', 1);

    client.removeAllListeners('value');
    client.close();
  });

  it('can start an acquisition', async function()  {
    this.timeout(10000);
    const router = new VueRouter({
      routes: [ {
        path: '/', name: 'Dashboard', component: Dashboard
      } ]
    });
    /* emulate our router */
    router.afterEach((to) => { /* jshint unused:false */
      store.commit('queryChange', to.query);
    });
    /* fix issue with vue-test-utils 31, router is not updated for some reasons */
    router.push = _.wrap(router.push, function(p, arg) {
      router.app.$nextTick(() => p.call(router, arg));
    });
    wrapper = mount({ template: '<router-view />' }, { router });

    router.push({ query: { dns: env.dns } });

    await waitFor(wrapper, () => wrapper.findComponent({ name: 'SidebarContent' }).exists());
    var sidebar = wrapper.findComponent({ name: 'SidebarContent' });
    expect(sidebar.exists()).to.be.true();
    await waitFor(sidebar, () => sidebar.findAll('.daq').length >= 1);

    wrapper.findComponent({ name: 'SidebarContent' }).findAll('.daq')
    .filter((b) => b.text() === 'mydaq')
    .trigger("click");

    /* ensure daq is connected */
    var daq = wrapper.findComponent({ name: 'Daq' });
    expect(daq.exists()).to.be.true();
    expect(daq.vm.dns).to.equal(env.dns);

    await waitFor(daq, () => daq.find('.fa-cog').exists());
    daq.find('.fa-cog').trigger('click');
    await waitFor(daq, () => daq.vm.inEdit);

    /* configure cards */
    let button = await waitForWrapper(daq, () => {
      return daq.findAll('button:not(disabled)')
      .filter((b) => b.text() === 'Configure');
    });
    button.trigger('click');

    /* start acquisition $FlowIgnore */
    const runNum = await waitForWrapper(daq, () => daq.findComponent({ name: 'AppRunNumberParam' }));
    runNum.find('input').setValue('1234');

    button = await waitForWrapper(wrapper, () => {
      return wrapper.findAll('button:not(disabled)')
      .filter((b) => b.text() === 'Start');
    });
    button.trigger('click');
    await waitFor(daq, () => daq.vm.isRunning, 3000);
  });
});
