
import Vue from 'vue';
import { config as tuConfig } from '@vue/test-utils';
import { before } from 'mocha';

import { default as BaseVue, BaseLogger as logger } from "@cern/base-vue";

import VueUtils from '../src/VueUtils';
import { Area } from '../src/Consts';
import store from '../src/store';

import d from 'debug';
const debug = d('test:error');

before(function() {
  Vue.use(VueUtils);
  Vue.use(BaseVue);

  /* "provide" doesn't seem to work properly, using mocks */
  tuConfig.mocks['$store'] = store;

  // Override default DNS values
  Area.EAR1.dns = 'ear1-fake-url.cern.ch';
  Area.EAR2.dns = 'ear2-fake-url.cern.ch';
  Area.LAB.dns = 'lab-fake-url.cern.ch';

  logger.on('error', (error) => debug('error:', error));
});
