//@flow
import './karma_index';
import _ from 'lodash';

import { afterEach, describe, it } from 'mocha';
import { expect } from 'chai';
import { mount } from '@vue/test-utils';

/* $FlowIgnore */
import RFMergerStatsView from '../src/components/RFMerger/RFMergerStatsView';
import { RFMergerStats } from '../src/interfaces/rfmerger';

/*::
declare var serverRequire: (string) => any
*/

describe('RFMergerStatsView', function() {
  let wrapper/*: Vue$Wrapper */;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* $FlowIgnore */
      wrapper = null;
    }
  });

  it('not initialized state', function() {
    wrapper = mount(RFMergerStatsView, {
      propsData: { stats: null, inEdit: false }
    });
    expect(wrapper.findAllComponents({ name: 'BaseParamReadonly' }).length).to.equal(0);
  });

  it('initialized state', function() {
    const stats = new RFMergerStats();
    _.forEach(stats, (value, key) => {
      _.set(stats, key, 10);
    });
    wrapper = mount(RFMergerStatsView, {
      propsData: { stats: stats, inEdit: false }
    });
    expect(wrapper.findAllComponents({ name: 'BaseParamReadonly' }).length).to.be.above(0);
  });
});
