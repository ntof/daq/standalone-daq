//@flow
import './karma_index';
import { waitFor } from './utils';
import * as utilities from "../src/utilities";
import { BaseLogger as logger } from '@cern/base-vue';

import { afterEach, beforeEach, describe, it } from 'mocha';
import { expect } from 'chai';
import _ from "lodash";
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';

import store from '../src/store';
import Daq from '../src/components/Daq';

/*::
declare var serverRequire: (string) => any
*/

describe('DaqState', function() {
  let wrapper/*: Vue$Wrapper */;
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');
      const { DnsServer } = serverRequire('@cern/dim');
      const { DisXmlNode } = serverRequire('@ntof/dim-xml');
      const { DaqStub } = serverRequire('@ntof/ntof-stubs');

      this.env = {};
      return utils.createApp(this.env)
      .then(() => {
        this.env.dns = new DnsServer(null, 0);
        return this.env.dns.listen();
      })
      .then(() => {
        this.env.node = new DisXmlNode(null, 0);
        this.env.stub = new DaqStub(this.env.dns.url());
        this.env.stub.register('mydaq', this.env.node);
        return this.env.node.register(this.env.dns.url());
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          server: this.env.server,
          dns: this.env.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(function() {
    return server.run(function() {
      this.env.server.close();
      this.env.node.close();
      this.env.dns.close();
      this.env.stub.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* $FlowIgnore */
      wrapper = null;
    }
  });

  it('displays daq information', async function() {
    store.commit('queryChange', { dns: env.dns, daq: 'mydaq' });
    wrapper = mount(Daq, { propsData: { canConfigure: env.canConfigure } });

    await waitFor(wrapper, () => !_.isNil(wrapper.vm.daqState));
    expect(wrapper.vm.daqState).to.equal(0);

    /* update the stub values */
    await server.run(function() {
      this.env.stub.daqState.setState(1);
    });

    /* ensure updates are being displayed on front-end */
    await waitFor(wrapper, () => (wrapper.vm.daqState === 1));
  });

  it('reports an error if dns doesn\'t exist', async function() {
    store.commit('queryChange', { dns: 'this is not the dns you are looking for', daq: 'mydaq' });
    this.timeout(10000);
    logger.clear();
    wrapper = mount(Daq, { propsData: { canConfigure: env.canConfigure } });

    await waitFor(wrapper, () => logger.getErrors().length > 0, 5000);
    expect(_.first(logger.getErrors())).to.have.property('message')
    .that.contains('disconnected');
    await waitFor(wrapper, () => wrapper.vm.daqState === null);
  });

  it('reports an error if server crashes', async function() {
    store.commit('queryChange', { dns: env.dns, daq: 'mydaq' });
    this.timeout(10000);
    logger.clear();
    await server.run(function() {
      this.env.server.close();
    });
    wrapper = mount(Daq, { propsData: { canConfigure: env.canConfigure } });

    await waitFor(wrapper, () => logger.getErrors().length > 0, 5000);
    expect(_.first(logger.getErrors())).to.have.property('message')
    .that.contains('NetworkError');
    await waitFor(wrapper, () => wrapper.vm.daqState === null);
  });

  it('enables edit by clicking on cog button', async () => {
    store.commit('queryChange', { dns: env.dns, daq: 'mydaq' });
    wrapper = mount(Daq, { propsData: { canConfigure: env.canConfigure } });
    wrapper.find('.ml-auto.clickable').trigger('click');
    await waitFor(wrapper, () => wrapper.vm.inEdit !== null);
    expect(wrapper.vm.inEdit).to.equal(true);
  });
});
