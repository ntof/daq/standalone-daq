//@flow
import './karma_index';
import { waitFor } from './utils';
import * as utilities from "../src/utilities";

import { afterEach, beforeEach, describe, it } from 'mocha';
import { expect } from 'chai';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';

import store from '../src/store';
import RFMergerGlobalStats from '../src/components/RFMerger/RFMergerGlobalStats';

/*::
declare var serverRequire: (string) => any
*/

describe('RFMergerGlobalStats', function() {
  let wrapper/*: Vue$Wrapper */;
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');
      const { DnsServer } = serverRequire('@cern/dim');
      const { DisXmlNode } = serverRequire('@ntof/dim-xml');
      const { RFMergerStub } = serverRequire('@ntof/ntof-stubs');

      this.env = {};
      return utils.createApp(this.env)
      .then(() => {
        this.env.dns = new DnsServer(null, 0);
        return this.env.dns.listen();
      })
      .then(() => {
        this.env.node = new DisXmlNode(null, 0);
        this.env.rfm = new RFMergerStub(this.env.dns.url());
        this.env.rfm.register("MERGER", this.env.node);
        return this.env.node.register(this.env.dns.url());
      })
      .then(() => {
        this.env.rfm.run();
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          server: this.env.server,
          dns: this.env.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(function() {
    return server.run(function() {
      this.env.rfm.close();
      this.env.server.close();
      this.env.node.close();
      this.env.dns.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* $FlowIgnore */
      wrapper = null;
    }
  });

  it('not initialized state', function() {
    store.commit('queryChange', { dns: null });
    wrapper = mount(RFMergerGlobalStats, { propsData: { inEdit: false } });
    expect(wrapper.findAllComponents({ name: 'BaseParamReadonly' }).length).to.equal(0);
  });

  it('initialized state', async function() {
    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(RFMergerGlobalStats, { propsData: { inEdit: false } });
    await waitFor(wrapper, () => !wrapper.vm.infoLoading, 2000);
    expect(wrapper.findAllComponents({ name: 'BaseParamReadonly' }).length).to.be.above(0);
    expect(JSON.stringify(wrapper.emitted())).to.include('stats-updated');
  });
});
