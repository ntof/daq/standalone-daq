//@flow
import './karma_index';
import { waitFor } from './utils';
import * as utilities from "../src/utilities";

import { afterEach, beforeEach, describe, it } from 'mocha';
import { expect } from 'chai';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';

import store from '../src/store';
import RFMerger from '../src/components/RFMerger/RFMerger';

/*::
declare var serverRequire: (string) => any
*/

describe('RFMerger', function() {
  let wrapper/*: Vue$Wrapper */;
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');
      const { DnsServer } = serverRequire('@cern/dim');
      const { DisXmlNode } = serverRequire('@ntof/dim-xml');
      const { RFMergerStub } = serverRequire('@ntof/ntof-stubs');

      this.env = {};
      return utils.createApp(this.env)
      .then(() => {
        this.env.dns = new DnsServer(null, 0);
        return this.env.dns.listen();
      })
      .then(() => {
        this.env.node = new DisXmlNode(null, 0);
        this.env.rfm = new RFMergerStub(this.env.dns.url());
        this.env.rfm.register("MERGER", this.env.node);
        return this.env.node.register(this.env.dns.url());
      })
      .then(() => {
        this.env.rfm.run();
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          server: this.env.server,
          dns: this.env.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(function() {
    return server.run(function() {
      this.env.rfm.close();
      this.env.server.close();
      this.env.node.close();
      this.env.dns.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* $FlowIgnore */
      wrapper = null;
    }
  });

  it('not initialized state', async function() {
    /* dumb test, just validate that workers are working fine */
    store.commit('queryChange', { dns: null });
    wrapper = mount(RFMerger);

    await waitFor(wrapper, () => wrapper.findComponent({ ref: "rfmState" }).exists());
    // Only rfmState
    expect(wrapper.findComponent({ ref: "rfmState" }).exists()).to.be.true();
    expect(wrapper.findComponent({ ref: "rfmCurrentRun" }).exists()).to.be.true();
    expect(wrapper.findComponent({ ref: "rfmRuns" }).exists()).to.be.false();
    expect(wrapper.findComponent({ ref: "rfmInfo" }).exists()).to.be.false();
  });

  it('initialized state', async function() {
    /* dumb test, just validate that workers are working fine */
    store.commit('queryChange', { dns: env.dns });
    wrapper = mount(RFMerger);

    await waitFor(wrapper, () => wrapper.vm.globalRate !== -1, 2000);
    expect(wrapper.findComponent({ ref: "rfmState" }).exists()).to.be.true();
    expect(wrapper.findComponent({ ref: "rfmCurrentRun" }).exists()).to.be.true();
    expect(wrapper.findComponent({ ref: "rfmRuns" }).exists()).to.be.false(); // lazy-loaded
    expect(wrapper.findComponent({ ref: "rfmInfo" }).exists()).to.be.true();
    expect(wrapper.vm.globalRate).to.equal(0);
    expect(wrapper.vm.globalTransferred).to.equal(0);
    expect(wrapper.vm.currentRunNumber).to.equal(900003);

    /* test lazy-loading for runs */
    wrapper.findAllComponents({ name: 'BaseCollapsible' }).setData({ isExpanded: true });
    await waitFor(wrapper, () => wrapper.findComponent({ ref: "rfmRuns" }).exists());
    /* Let's start an acquisition directly on the stub. Real dim cmd
      * will be tested on RFMergerRuns*/
    await server.run(function() {
      return this.env.rfm._engine.updateRun(900003, "test", 1);
    });
    await waitFor(wrapper, () => wrapper.vm.globalRate > 0, 5000);
    expect(wrapper.vm.globalRate).to.be.above(0);
    expect(wrapper.vm.globalTransferred).to.be.above(0);
    expect(wrapper.vm.currentRunNumber).to.equal(900003);
  });
});
