//@flow
import './karma_index';

import { afterEach, beforeEach, describe, it } from 'mocha';
import { expect } from "chai";
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';
import { invoke } from 'lodash';

import { waitFor, waitForWrapper } from './utils';
import * as utilities from "../src/utilities";

import store from '../src/store';
import CardList from '../src/components/CardList';

/*::
declare var serverRequire: (string) => any
*/

describe('Card', function() {
  let wrapper/*: Vue$Wrapper */;
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');
      const { DnsServer } = serverRequire('@cern/dim');
      const { DisXmlNode } = serverRequire('@ntof/dim-xml');
      const { DaqStub } = serverRequire('@ntof/ntof-stubs');

      this.env = {};
      return utils.createApp(this.env)
      .then(() => {
        this.env.dns = new DnsServer(null, 0);
        return this.env.dns.listen();
      })
      .then(() => {
        this.env.node = new DisXmlNode(null, 0);
        this.env.daq = new DaqStub(this.env.dns.url());
        this.env.daq.run();
        this.env.daq.register("mydaq", this.env.node);
        return this.env.node.register(this.env.dns.url());
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          server: this.env.server,
          dns: this.env.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(function() {
    return server.run(function() {
      this.env.daq.close();
      this.env.server.close();
      this.env.node.close();
      this.env.dns.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* $FlowIgnore */
      wrapper = null;
    }
  });

  it('displays calibration', async function() {
    store.commit('queryChange', { dns: env.dns, daq: 'mydaq' });
    wrapper = mount(CardList);

    await waitFor(wrapper, () => !wrapper.vm.isLoading);
    invoke(wrapper.find('.x-calib'), [ 'vm', 'show' ]);

    const chan = await waitForWrapper(wrapper,
      () => wrapper.findComponent({ name: 'ChannelCalibration' }));

    const params = chan.findAllComponents({ name: 'BaseParamReadonly' });
    expect(params.length).to.equal(4);

    const fullScale = params.filter((p) => p.vm.title === 'Calibrated Full Scale [mV]').at(0);
    const offset = params.filter((p) => p.vm.title === "Calibrated Offset [mV]").at(0);
    const threshold = params.filter((p) => p.vm.title === "Calibrated Treshold [mV]").at(0);

    await waitFor(chan, () => fullScale.vm.value > 0);
    expect(fullScale.vm.value).to.equal(5050);
    expect(offset.vm.value).to.equal(50);
    expect(threshold.vm.value).to.be.closeTo(130, 1);

    /* edit threshold and configure to update the value */
    await waitFor(wrapper, () => wrapper.vm.canConfigure);
    wrapper.find('.fa-cog').trigger('click');
    await waitFor(wrapper, () => wrapper.vm.inEdit);

    const thresholdEdit = wrapper.find('.x-zsp').findAll('.b-param')
    .filter((p) => p.vm.title === 'Threshold [mV]').at(0);
    thresholdEdit.find('input').setValue('500');

    // @edit event are async, let's wait for updates
    await wrapper.vm.$nextTick();

    wrapper.findAll('button').filter((b) => b.text() === 'Configure')
    .trigger('click');
    /* ensure that it has changed and is way above initial value */
    await waitFor(wrapper, () => threshold.vm.value > 450);
  });
});
