
const
  q = require('q'),

  Server = require('../../src/Server');

function createApp(env) {
  var def = q.defer();

  env.server = new Server({ port: 0, basePath: '' });

  env.server.listen(() => def.resolve());
  return def.promise;
}

module.exports = { createApp };
