//@flow
import './karma_index';
import { waitFor, waitForValue } from './utils';
import * as utilities from "../src/utilities";
import { BaseLogger as logger } from '@cern/base-vue';

import { afterEach, beforeEach, describe, it } from 'mocha';
import { expect } from 'chai';
import _ from 'lodash';
import { mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';

import store from '../src/store';
import Acq from '../src/components/Daq';

/*::
declare var serverRequire: (string) => any
*/

describe('AcqState', function() {
  let wrapper/*: Vue$Wrapper */;
  let env;

  beforeEach(function() {
    this.timeout(10000);
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');
      const { DnsServer } = serverRequire('@cern/dim');
      const { DisXmlNode } = serverRequire('@ntof/dim-xml');
      const { DaqStub } = serverRequire('@ntof/ntof-stubs');

      this.env = {};
      return utils.createApp(this.env)
      .then(() => {
        this.env.dns = new DnsServer(null, 0);
        return this.env.dns.listen();
      })
      .then(() => {
        this.env.node = new DisXmlNode(null, 0);
        this.env.stub = new DaqStub(this.env.dns.url());
        this.env.stub.register('mydaq', this.env.node);
        return this.env.node.register(this.env.dns.url());
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          server: this.env.server,
          dns: this.env.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(function() {
    return server.run(function() {
      this.env.server.close();
      this.env.node.close();
      this.env.dns.close();
      this.env.stub.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* $FlowIgnore */
      wrapper = null;
    }
  });

  it('displays acq state information', async function() {
    store.commit('queryChange', { dns: env.dns, daq: 'mydaq' });
    wrapper = mount(Acq, { propsData: { canConfigure: env.canConfigure } });

    await waitFor(wrapper, () => !_.isNil(wrapper.vm.acqState));
    expect(wrapper.vm.acqState).to.equal(0);
    /* update the stub values */
    await server.run(function() {
      this.env.stub.acqState.setState(1);
    });
    /* ensure updates are being displayed on front-end */
    await waitFor(wrapper, () => (wrapper.vm.acqState !== 0));
  });

  it('displays an error when service crashes', async function() {
    store.commit('queryChange', { dns: env.dns, daq: 'mydaq' });
    wrapper = mount(Acq, { propsData: { canConfigure: env.canConfigure } });

    await waitFor(wrapper, () => !_.isNil(wrapper.vm.acqState));
    await server.run(function() {
      this.env.node.close();
    });
    await waitFor(wrapper, () => _.isNil(wrapper.vm.acqState));
    expect(wrapper.find('.alert h6').text())
    .to.contains('Waiting for service');
    /* reconnect */
    await server.run(function() {
      const { DisXmlNode } = serverRequire('@ntof/dim-xml');
      this.env.node = new DisXmlNode(null, 0);
      this.env.stub.register('mydaq', this.env.node);
      return this.env.node.register(this.env.dns.url())
      .thenResolve(undefined);
    });
    await waitFor(wrapper, () => waitFor(wrapper, () => !_.isNil(wrapper.vm.acqState)));
  });

  it('reports an error if dns doesn\'t exist', async function() {
    store.commit('queryChange', { dns: 'this is not the dns you are looking for', daq: 'mydaq' });
    this.timeout(10000);
    logger.clear();
    wrapper = mount(Acq, { propsData: { canConfigure: env.canConfigure } });
    await waitFor(wrapper, () => logger.getErrors().length > 0, 6000);
    expect(_.first(logger.getErrors())).to.have.property('message')
    .that.contains('disconnected');
    await waitForValue(wrapper, () => wrapper.vm.acqState, undefined);
  });

  it('reports an error if server crashes', async function() {
    store.commit('queryChange', { dns: env.dns, daq: 'mydaq' });
    logger.clear();
    await server.run(function() {
      this.env.server.close();
    });
    wrapper = mount(Acq, { propsData: { canConfigure: env.canConfigure } });

    await waitFor(wrapper, () => logger.getErrors().length > 0);
    expect(_.first(logger.getErrors())).to.have.property('message')
    .that.contains('NetworkError');
    await waitFor(wrapper, () => wrapper.vm.acqState === undefined);
  });

  it('can reconnect to service', async function() {
    store.commit('queryChange', { dns: env.dns, daq: 'mydaq' });
    wrapper = mount(Acq, { propsData: { canConfigure: env.canConfigure } });

    await waitFor(wrapper, () => !_.isNil(wrapper.vm.acqState));
    expect(wrapper.vm.acqState).to.equal(0);
    await server.run(function() {
      this.env.node.close();
    });
    await waitFor(wrapper, () => _.isNil(wrapper.vm.acqState));
    expect(wrapper.find('.alert h6').text())
    .to.contains('Waiting for service');
    /* reconnect */
    await server.run(function() {
      const { DisXmlNode } = serverRequire('@ntof/dim-xml');
      this.env.node = new DisXmlNode(null, 0);
      this.env.stub.register('mydaq', this.env.node);
      return this.env.node.register(this.env.dns.url()).thenResolve(undefined);
    });
    await waitFor(wrapper, () => !_.isNil(wrapper.vm.acqState));
    /* update the stub values */
    await server.run(function() {
      this.env.stub.acqState.setState(1);
    });
    /* ensure updates are being displayed on front-end */
    await waitFor(wrapper, () => (wrapper.vm.acqState === 1));
  });
});
