//@flow
import './karma_index';
import { waitFor } from './utils';
import * as utilities from "../src/utilities";

import { afterEach, beforeEach, describe, it } from 'mocha';
import { expect } from 'chai';
import { createWrapper, mount } from '@vue/test-utils';
import server from '@cern/karma-server-side';

import store from '../src/store';
import Daq from '../src/components/Daq';

/*::
declare var serverRequire: (string) => any
*/

describe('Daq', function() {
  let wrapper/*: Vue$Wrapper */;
  let env;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');
      const { DnsServer } = serverRequire('@cern/dim');
      const { DisXmlNode } = serverRequire('@ntof/dim-xml');
      const { DaqStub } = serverRequire('@ntof/ntof-stubs');

      this.env = {};
      return utils.createApp(this.env)
      .then(() => {
        this.env.dns = new DnsServer(null, 0);
        return this.env.dns.listen();
      })
      .then(() => {
        this.env.node = new DisXmlNode(null, 0);
        this.env.daq = new DaqStub(this.env.dns.url());
        this.env.daq.register("mydaq", this.env.node);
        return this.env.node.register(this.env.dns.url());
      })
      .then(() => {
        this.env.daq.run();
      })
      .then(() => {
        var addr = this.env.server.address();
        return {
          server: this.env.server,
          dns: this.env.dns.url().substr(6),
          proxyUrl: `http://localhost:${addr.port}`
        };
      });
    })
    .then((ret) => {
      utilities.setCurrentUrl(ret.proxyUrl);
      env = ret;
    });
  });

  afterEach(function() {
    return server.run(function() {
      this.env.daq.close();
      this.env.server.close();
      this.env.node.close();
      this.env.dns.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* $FlowIgnore */
      wrapper = null;
    }
  });

  it('can reset a Daq', async function() {
    store.commit('queryChange', { dns: env.dns, daq: 'mydaq' });
    wrapper = mount(Daq, { propsData: { canConfigure: env.canConfigure } });
    const stateWrapper = createWrapper(wrapper.vm.$refs.daqState);
    await waitFor(wrapper, () => wrapper.vm.daqState !== null, 2000);
    wrapper.find('.ml-auto.clickable').trigger('click');
    wrapper.find('.btn.btn-secondary').trigger('click');
    await waitFor(wrapper,
      () => expect(JSON.stringify(stateWrapper.emitted())).to.include("Reseting"));
  });
});
