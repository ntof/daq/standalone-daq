//@flow
import './karma_index';

import { afterEach, describe, it } from 'mocha';
import { expect } from "chai";
import { mount } from '@vue/test-utils';

import { waitFor } from './utils';
/* $FlowIgnore */
import RunNumberParam from '../src/components/AppRunNumberParam';

/*::
declare var serverRequire: (string) => any
*/

describe('RunNumberParam', function() {
  let wrapper/*: Vue$Wrapper */;

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      /* $FlowIgnore */
      wrapper = null;
    }
  });

  it('updates area prefix', async function() {
    wrapper = mount(RunNumberParam, { propsData: { inEdit: true } });

    await waitFor(wrapper, () => wrapper.find('select').exists());
    wrapper.find('input').setValue('100001');
    await wrapper.vm.$nextTick();
    let sel = wrapper.find('select');
    expect(sel)
    .to.have.nested.property('element.value', '1');
    expect(!wrapper.find('.alert-warning').exists());
    expect(!wrapper.find('.alert-danger').exists());

    wrapper.find('input').setValue('500001');
    await wrapper.vm.$nextTick();
    sel = wrapper.find('select');
    expect(sel)
    .to.have.nested.property('element.value', '0');
    expect(wrapper.find('.alert-warning').exists());
    expect(!wrapper.find('.alert-danger').exists());
  });

  it('updates value according to prefix', async function() {
    wrapper = mount(RunNumberParam, { propsData: { inEdit: true } });

    await waitFor(wrapper, () => wrapper.find('select').exists());
    wrapper.find('input').setValue('1234');
    await wrapper.vm.$nextTick();
    wrapper.find('select').setValue('9');
    await wrapper.vm.$nextTick();
    expect(wrapper.find('input'))
    .to.have.nested.property('element.value', '901234');
  });

  function testError(value, selector) {
    it(`displays an alert for "${value}"`, async () => {
      wrapper = mount(RunNumberParam, { propsData: { inEdit: true } });

      await waitFor(wrapper, () => wrapper.find('select').exists());
      wrapper.find('input').setValue(value);
      await waitFor(wrapper, () => wrapper.find(selector).exists());
    });
  }

  testError(100000, '.alert-danger'); /* runnum must be != 0 */
  testError(0, '.alert-danger'); /* runnum must be != 0 */
  testError(800001, '.alert-warning'); /* unknown area warning */
});
