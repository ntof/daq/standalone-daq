//@flow
import './karma_index';
import { describe, it } from 'mocha';
import { expect } from 'chai';

import { ZSPConfig } from '../src/ZeroSuppressionConfig';
import { DicXmlDataSet } from '@ntof/redim-client';


const sampleConfig = [
  { index: 1, name: 'mode', type: DicXmlDataSet.DataType.STRING, value: 'master' },
  { index: 2, name: 'configuration', value: [
    { index: 0, name: 'master', value: [
      { index: 0, name: 'sn', type: DicXmlDataSet.DataType.STRING, value: '1234' },
      { index: 1, name: 'channel', type: DicXmlDataSet.DataType.UINT32, value: 0 },
      { index: 2, name: 'slave', value: [
        { index: 0, name: 'sn', type: DicXmlDataSet.DataType.STRING, value: '5678' },
        { index: 1, name: 'channel', type: DicXmlDataSet.DataType.UINT32, value: 1 }
      ] },
      { index: 3, name: 'slave', value: [
        { index: 0, name: 'sn', type: DicXmlDataSet.DataType.STRING, value: '1234' },
        { index: 1, name: 'channel', type: DicXmlDataSet.DataType.UINT32, value: 1 }
      ] },
      { index: 4, name: 'slave', value: [
        { index: 0, name: 'sn', type: DicXmlDataSet.DataType.STRING, value: '1234' },
        { index: 1, name: 'channel', type: DicXmlDataSet.DataType.UINT32, value: 2 }
      ] }
    ] }
  ] }
];

describe('ZeroSuppressionConfig', function() {
  it('can load a configuration', function() {
    var zsp = ZSPConfig.fromParam(sampleConfig);
    if (!zsp) { throw 'failed to parse'; } /* for flow */

    expect(zsp).to.be.instanceOf(ZSPConfig);
    expect(zsp.master).to.have.length(1);

    expect(zsp.master[0].slave)
    .to.deep.equal([
      { sn: '1234', channel: 1, masterId: '1234:0', id: '1234:1' },
      { sn: '1234', channel: 2, masterId: '1234:0', id: '1234:2' },
      { sn: '5678', channel: 1, masterId: '1234:0', id: '5678:1' }
    ]);
  });

  it('can move a slave', function() {
    var zsp = ZSPConfig.fromParam(sampleConfig);
    if (!zsp) { throw 'failed to parse'; } /* for flow */

    zsp.moveSlave(zsp.master[0].slave[1], null); /* move as indep */
    expect(zsp.master).to.have.length(1);
    expect(zsp.master[0].slave).to.have.length(2);

    expect(zsp.indep)
    .to.deep.equal([ { sn: '1234', channel: 2, id: '1234:2' } ]);

    zsp.moveSlave(zsp.indep[0], zsp.master[0].id);
    expect(zsp.master[0].slave)
    .to.deep.equal([
      { sn: '1234', channel: 1, masterId: '1234:0', id: '1234:1' },
      { sn: '1234', channel: 2, masterId: '1234:0', id: '1234:2' },
      { sn: '5678', channel: 1, masterId: '1234:0', id: '5678:1' }
    ]);
  });
});
