//@flow

/* must be done early */
import debug from 'debug';

debug.useColors = () => false;

localStorage['debug'] = '';

import chai from 'chai';
import { before, beforeEach } from 'mocha';
import server from '@cern/karma-server-side';
import dirtyChai from 'dirty-chai';

import store from '../src/store';

/* force include router for coverage */
import '../src/router/index';
import './karma_init';

chai.use(dirtyChai);

before(function() {
  return server.run(function() {
    // add a guard to not add one event listener per test
    if (!this.karmaInit) {
      // $FlowIgnore
      this.karmaInit = true;
      /* do not accept unhandledRejection */
      process.on('unhandledRejection', function(reason) {
        console.log('UnhandledRejection:', reason);
        throw reason;
      });
    }

    return process.env['DEBUG'];
  })
  .then((dbg) => {
    localStorage['debug'] = dbg;
  });
});

beforeEach(function() {
  store.commit('clear');
});
