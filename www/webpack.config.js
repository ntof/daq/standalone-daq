const
  path = require('path'),
  child = require('child_process'),
  webpack = require('webpack'),
  _ = require('lodash'),
  { execSync } = require('child_process'),
  VueLoaderPlugin = require('vue-loader/lib/plugin'),
  CompressionPlugin = require('compression-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: path.resolve('./src/index.js'),
  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: '/dist/',
    filename: 'index.js',
    hashFunction: 'xxhash64'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [ 'style-loader', 'css-loader' ]
      },
      {
        test: /\.scss$/,
        use: [ 'style-loader', 'css-loader', 'sass-loader' ]
      },
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: { }
        }
      },
      {
        test: /\.pug$/,
        loader: 'pug-plain-loader'
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        options: {
          extends: path.join(__dirname, '.babelrc')
        }
      },
      {
        test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
        type: 'asset/resource',
        generator: {
          filename: 'fonts/[name][ext]'
        }
      },
      {
        test: /\.shared-worker\.js/,
        loader: 'worker-loader',
        options: {
          filename: '[name].js',
          worker: {
            type: 'SharedWorker',
            options: { name: 'worker' }
          }
        }
      }
    ]
  },
  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js',
      'bootstrap': 'bootstrap/dist/js/bootstrap.bundle.min.js'
    },
    extensions: [ '.js', '.vue', '.json' ]
  },
  devServer: {
    historyApiFallback: true,
    noInfo: true,
    overlay: true
  },
  performance: {
    hints: "warning"
  },
  devtool: 'inline-cheap-module-source-map',
  plugins: [
    new webpack.ProvidePlugin({
      Buffer: [ 'buffer', 'Buffer' ]
    }),
    new VueLoaderPlugin(),
    new webpack.DefinePlugin({
      VERSION: JSON.stringify(child.execSync('git describe --tags --always HEAD').toString().trim())
    }),
    {
      apply: (compiler) => {
        compiler.hooks.beforeCompile.tap('IstanbulPatch', () => {
          /* see https://github.com/istanbuljs/nyc/issues/718 for details */
          execSync("sed -i='tmp' 's/source: pathutils.relativeTo(start.source, origFile),/source: origFile,/' node_modules/istanbul-lib-source-maps/lib/get-mapping.js")
        });
      }
    }
  ]
};

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = 'source-map';
  module.exports.mode = 'production';
  _.set(module.exports, 'optimization.nodeEnv', 'production');
  _.set(module.exports, 'optimization.moduleIds', 'named');

  module.exports.plugins.push(
    new CompressionPlugin({ algorithm: 'gzip', threshold: 10240, minRatio: 0.8 }),
    new CompressionPlugin({ algorithm: 'brotliCompress', filename: "[path][base].br", threshold: 10240, minRatio: 0.8 }));
}
