//@flow

import { DicXmlCmd } from '@ntof/redim-client';
import { UrlUtilitiesMixin, errorPrefix } from '../utilities';
import Vue from 'vue';

const wrapError = errorPrefix.bind(null, 'Daq: ');

/**
 * @typedef {V.Instance<typeof component> &
 *  V.Instance<typeof UrlUtilitiesMixin>} Instance
 */

const component = Vue.extend({
  mixins: [ UrlUtilitiesMixin ],
  methods: {
    /**
     * @this {Instance}
     * @param {string} daq
     * @param {string} dns
     * @return {Promise<void>}
     */
    daqStart(daq /*: string */, dns /*: string */) {
      return DicXmlCmd.invoke(daq + '/Daq/Command',
        { command: { command: 'start' } },
        { proxy: this.getProxy() }, this.dnsUrl(dns))
      .catch(wrapError);
    },
    /**
     * @this {Instance}
     * @param {string} daq
     * @param {string} dns
     * @return {Promise<void>}
     */
    daqStop(daq /*: string */, dns /*: string */) {
      return DicXmlCmd.invoke(daq + '/Daq/Command',
        { command: { command: 'stop' } },
        { proxy: this.getProxy() },  this.dnsUrl(dns))
      .catch(wrapError);
    },
    /**
     * @this {Instance}
     * @param {string} daq
     * @param {string} dns
     * @return {Promise<void>}
     */
    daqReset(daq /*: string */, dns /*: string */) {
      return DicXmlCmd.invoke(daq + '/Daq/Command',
        { command: { command: 'reset' } },
        { proxy: this.getProxy() },  this.dnsUrl(dns))
      .catch(wrapError);
    },
    /**
     * @this {Instance}
     * @param {string} daq
     * @param {string} dns
     * @return {Promise<void>}
     */
    daqCalibrate(daq /*: string */, dns /*: string */) {
      return DicXmlCmd.invoke(daq + '/Daq/Command',
        { command: { command: 'calibrate' } },
        { proxy: this.getProxy() },  this.dnsUrl(dns))
      .catch(wrapError);
    }
  }
});
export default component;
