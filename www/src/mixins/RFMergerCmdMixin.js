// @ts-check

import { DicXmlCmd } from '@ntof/redim-client';
import { UrlUtilitiesMixin, errorPrefix } from '../utilities';
import { isNil, map } from 'lodash';
import Vue from 'vue';

const wrapError = errorPrefix.bind(null, 'RawFileMerger: ');
/**
 * @typedef {V.Instance<typeof component> &
  *  V.Instance<typeof UrlUtilitiesMixin>} Instance
  */

const component = Vue.extend({
  mixins: [ UrlUtilitiesMixin ],
  methods: {
    /**
     * @this {Instance}
     * @param  {number} runNumber
     * @param  {?string} experiment
     * @param  {boolean} approved
     * @param  {boolean} standalone
     * @param  {Array<number>} daqs
     * @param  {string} dns
     * @return {Promise<any>}
     */
    rfmStart(runNumber /*: number */,
      experiment /*: ?string */,
      approved /*: boolean */,
      standalone /*: boolean */,
      daqs /*: Array<number> */,
      dns /*: string */) {

      /** @type {{name: string, runNumber: number, approved: number,
        standalone: number,experiment?:string}} */
      const args /*: any */ = {
        name: 'runStart', runNumber,
        approved: approved ? 1 : 0,
        standalone: standalone ? 1 : 0
      };

      if (!isNil(experiment)) {
        args['experiment'] = experiment;
      }
      return DicXmlCmd.invoke('MERGER',
        {
          command: {
            '$': args,
            daq: map(daqs, (id) => ({ '$': { crateId: id } }))
          }
        },
        { proxy: this.getProxy() }, this.dnsUrl(dns))
      .catch(wrapError);
    },

    /**
     * @this {Instance}
     * @param  {number} runNumber
     * @param  {string} dns
     * @return {Promise<any>}
     */
    rfmStop(runNumber /*: number */, dns /*: string */) {
      return DicXmlCmd.invoke('MERGER',
        { command: { '$': { name: 'runStop', runNumber: runNumber } } },
        { proxy: this.getProxy() }, this.dnsUrl(dns))
      .catch(wrapError);
    },

    /**
     * @this {Instance}
     * @param  {number} runNumber
     * @param  {string} dns
     * @return {Promise<any>}
     */
    rfmDelete(runNumber /*: number */, dns /*: string */) {
      return DicXmlCmd.invoke('MERGER',
        { command: { '$': { name: 'runDelete', runNumber: runNumber } } },
        { proxy: this.getProxy() }, this.dnsUrl(dns))
      .catch(wrapError);
    },

    /**
     * @this {Instance}
     * @param  {number} runNumber
     * @param  {number} fileNumber
     * @param  {?boolean} ignore
     * @param  {string} dns
     * @return {Promise<any>}
     */
    rfmFileIgnore(runNumber /*: number */, fileNumber /*: number */, ignore /*: ?boolean */, dns /*: string */) {
      return DicXmlCmd.invoke('MERGER',
        { command: { '$': {
          name: 'fileIgnore', runNumber: runNumber, fileNumber: fileNumber,
          ignore: (isNil(ignore) || ignore) ? 1 : 0
        } } },
        { proxy: this.getProxy() }, this.dnsUrl(dns))
      .catch(wrapError);
    }
  }
});
export default component;
