// @ts-check

import { find, get, isNil, keyBy, toNumber, toString } from 'lodash';
import {
  BaseAnimationBlock as AnimBlock,
  BaseParamBase as BaseParam } from '@cern/base-vue';
import { genId } from '../utilities';
import { AreaList } from '../Consts';
import Vue from 'vue';
import { mapState } from 'vuex';

/**
 * @typedef {{ base: V.Instance<typeof BaseParam, Vue> }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<typeof options, Refs>>} Instance
 */

const PrefixBase = 100000;
const options = {
  areaList: AreaList,
  prefixMap: keyBy(AreaList, 'prefix')
};

const component = /** @type {V.Constructor<typeof options, Refs>} */ (Vue).extend({
  name: 'AppRunNumberParam',
  components: { AnimBlock, BaseParam  },
  ...options,
  // areaList: AreaList,
  // prefixMap: keyBy(AreaList, 'prefix'),
  props: {
    inEdit: { type: Boolean, default: false },
    value: { type: [ String, Number ], default: undefined }
  },
  /**
   * @return {{
   *   editValue: ?(string|number),
   *   id: string
   * }}
   */
  data() { return { editValue: null, id: genId() }; },
  computed: {
    ...mapState([ 'dns' ]),
    prefix: {
      /**
       * @this {Instance}
       * @return {number}
       */
      get() {
        const area = this.getArea(Math.trunc(toNumber(this.getValue()) / PrefixBase));
        return isNil(area) ? 0 : area.prefix;
      },
      /**
       * @this {Instance}
       * @param {number} value
       */
      set(value /*: number */) {
        this.editValue =
          (toNumber(this.editValue) % PrefixBase) + (toNumber(value) * PrefixBase);
      }
    }
  },
  watch: {
    /** @this {Instance} */
    inEdit() {
      this.resetEditValue();
    },
    editValue() {
      this.checkValidity();
      this.$emit('edit', this.editValue);
    }
  },
  beforeMount() {
    this.$options.areaList = options.areaList;
    this.$options.prefixMap = options.prefixMap;
  },
  mounted() {
    this.resetEditValue();
  },
  methods: {
    /** @this {Instance} */
    resetEditValue() {
      /* we want to check for '' and 0 in addition to null/undefined */
      if (!this.value) {
        const area = this.getConnectedArea();
        this.editValue = isNil(area) ? '' : (area.prefix * PrefixBase);
      }
      else {
        this.editValue = toString(this.value);
      }
    },
    /** @this {Instance} */
    getValue() {
      return toNumber(this.inEdit ? this.editValue : this.value);
    },
    /** @this {Instance} */
    getConnectedArea() /*: ?{ prefix: number, name: string } */ {
      return find(AreaList, { dns: this.dns });
    },
    /**
     * @param  {?(number|string)=} prefix
     */
    getArea(prefix /*: ?(number|string) */) /*: ?{ name: string } */ {
      return this.$options.prefixMap[prefix || ''];
    },
    /**
     * @param  {?(number|string)=} prefix
     * @return {?string}
     */
    getAreaName(prefix /*: ?(number|string) */) /*: string */ {
      // @ts-ignore
      return get(this.$options.prefixMap, [ prefix, 'name' ], '?');
    },
    /** @this {Instance} */
    checkValidity() {
      this.$refs.base.removeError('a-wrong');
      this.$refs.base.removeError('a-invalid');
      this.$refs.base.removeWarning('a-unknown');

      const area = this.getConnectedArea();
      if (!isNil(area) && (area.prefix !== this.prefix)) {
        this.$refs.base.addError('a-wrong', `wrong prefix for ${area.name} area.`);
        return false;
      }

      const value = this.getValue();
      if (!value || (value % PrefixBase) === 0) {
        this.$refs.base.addError('a-invalid', 'enter a valid run number.');
        return false;
      }

      if (this.prefix === 0) {
        this.$refs.base.addWarning('a-unknown', 'unknown area prefix.');
      }
      return true;
    }
  }
});
export default component;
