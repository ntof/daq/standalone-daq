// @ts-check

import { get, invoke } from 'lodash';
import SidebarContent from './SidebarContent.vue';
import Timing from './Timing.vue';
import Daq from './Daq.vue';
import CardList from './CardList.vue';
import RFMerger from './RFMerger/RFMerger.vue';
import Vue from 'vue';
import { mapState } from 'vuex';

import { VERSION } from '../Consts';

/**
 * @typedef {V.Instance<typeof component> &
 * V.Instance<ReturnType<BaseKeyboardEventMixin>> } Instance
 */

const component = Vue.extend({
  name: 'Dashboard',
  components: {
    SidebarContent, Timing, Daq, CardList, RFMerger
  },
  /**
   * @return {{ isLoading: boolean, daqId: ?number, version: string, title: string }}
   */
  data() {
    return {
      isLoading: true, daqId: null, version: VERSION,
      title: 'Standalone DAQ'
    };
  },
  computed: {
    ...mapState({ dns: 'dns', daq: 'daqName' }),
    .../** @type {{ showKeyHints: boolean }} */mapState('ui', [ 'showKeyHints' ])
  },
  watch: {
    /** @this {Instance} */
    daq() {
      this.isLoading = true;
    }
  },
  /** @this {Instance} */
  mounted() {
    const sidebar = get(this.$refs, [ 'app', '$refs', 'sidebar' ]);
    if (sidebar) {
      sidebar.hidden = true;
    }
  },
  methods: {
    onConfigureEvent() {
      this.$refs.cards.configure(undefined, false);
    },
    /**
     * @param  {Event} [event]
     */
    toggleSideBar(event) {
      if (event) { event.preventDefault(); }
      invoke(this.$refs, [ 'app', '$refs', 'sidebar', 'toggle' ]);
    },
    onCardsLoaded() {
      this.isLoading = false;
      // grab first channel on first card chassisId
      this.daqId = get(this.$refs.cards,
        '$refs.Cards[0].$refs.Channels[0].chassisId');
    }
  }
});
export default component;
