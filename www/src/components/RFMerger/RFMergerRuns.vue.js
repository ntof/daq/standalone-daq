// @ts-check
import _ from 'lodash';
import { UrlUtilitiesMixin, getHumanReadableDate } from '../../utilities';
import { DicXmlParams } from '@ntof/redim-client';
import { RFMergerFilesView } from '@ntof/ntof-vue-widgets';
import {
  BaseAnimationBlock as AnimBlock,
  BaseCollapsible as Collapsible,
  BaseParamInput as Param,
  BaseParamDate as ParamDate,
  BaseParamReadonly as ParamReadonly,
  BaseLogger as logger } from '@cern/base-vue';
import RFMergerStatsView from './RFMergerStatsView.vue';
import RFMergerRunIcon from './RFMergerRunIcon.vue';
import { CurrentRunParamsMap, RFMergerRun, RunParamsMap } from '../../interfaces/rfmerger';
import { parseValue } from "../../interfaces";
import Vue from 'vue';
import { mapState } from 'vuex';

/**
 * @typedef {V.Instance<typeof component>
 *  & V.Instance<typeof UrlUtilitiesMixin>} Instance
 */

const component = Vue.extend({
  name: 'RFMergerRuns',
  components: { Collapsible, Param, ParamDate, ParamReadonly, AnimBlock,
    RFMergerStatsView, RFMergerFilesView, RFMergerRunIcon },
  mixins: [
    UrlUtilitiesMixin
  ],
  props: {
    inEdit: { type: Boolean, default: false },
    fromCurrentRun: { type: Boolean, default: false }
  },
  /**
   * @return {{
   *  client?: DicXmlParams,
   *  rfmRuns: Array<RFMergerRun>,
   *  loading: boolean
   * }}
   */
  data() {
    return { rfmRuns: [], loading: false };
  },
  computed: {
    ...mapState([ 'dns' ])
  },
  watch: {
    /** @this {Instance} */
    dns() { this.monitor(); }
  },
  /** @this {Instance} */
  mounted() {
    this.monitor();
  },
  /** @this {Instance} */
  beforeDestroy() {
    this.close();
  },
  methods: {
    /** @this {Instance} */
    close() {
      if (this.client) {
        this.client.removeListener('value', this.handleValueCurrentRun);
        this.client.removeListener('value', this.handleValueRuns);
        this.client.removeListener('error', this.handleError);
        this.client.close();
      }
      this.client = null;
    },
    /**
     * @this {Instance}
     * @param {number} runNumber
     */
    genId(runNumber /*: number*/) {
      return "fv_" + runNumber;
    },
    /** @this {Instance} */
    monitor() {
      this.close();
      // @ts-ignore: data is set
      _.assign(this, this.$options.data());
      if (_.isEmpty(this.dns)) { return; }
      this.loading = true;

      if (this.fromCurrentRun) {
        this.client = new DicXmlParams(`MERGER/Current`,
          { proxy: this.getProxy() }, this.dnsUrl(this.dns));
        this.client.on('error', this.handleError);
        this.client.on('value', this.handleValueCurrentRun);
        this.client.promise().catch(this.handleError);
      }
      else {
        this.client = new DicXmlParams('MERGER/Runs',
          { proxy: this.getProxy() },  this.dnsUrl(this.dns));
        this.client.on('error', this.handleError);
        this.client.on('value', this.handleValueRuns);
        this.client.promise().catch(this.handleError);
      }
    },
    /**
     * @this {Instance}
     * @param {any} value
     */
    handleValueCurrentRun(value /*: any */) {
      if (!value) { return; }
      this.rfmRuns = [];
      const currentRun = this.extractRunFromParams(value, CurrentRunParamsMap);
      this.rfmRuns.push(currentRun);
      this.loading = false;
      this.$emit('current-run', currentRun.runNumber);
    },
    /**
     * @this {Instance}
     * @param {Array<redim.XmlData.Data>} value
     */
    handleValueRuns(value /*: any */) {
      if (!value) { return; }
      /** @type {Array<RFMergerRun>} */
      const runs = [];
      _.forEach(value, (run, index) => {
        if (index === 0) { return; } // Number of Parameters param
        runs.push(this.extractRunFromParams(run.value, RunParamsMap));
      });
      // Order by DESC runNumber (higher run number first)
      this.rfmRuns = _.orderBy(runs, [ 'runNumber' ], [ 'desc' ]);
      this.loading = false;
    },

    /**
     * @this {Instance}
     * @param {{ [key: string]: string }} value
     *
     */
    extractRunFromParams(value /*: {} */, paramMap /*: RunParamsMapType | CurrentRunParamsMapType */) /*: RFMergerRun*/ {
      const vals = _.transform(value, (ret, val) => {
        ret[val.index] = val;
      }, []);

      const aRun = new RFMergerRun();
      _.forEach(paramMap, (param, name) => {
        _.set(aRun, name, parseValue(_.get(vals, [ param.idx, 'value' ], null), param.type));
      });
      return aRun;
    },
    /** @this {Instance} */
    handleError(err/*: any */) {
      logger.error(err);
    },
    /** @this {Instance} */
    getHumanizedDate(timestamp /*: number*/) {
      return getHumanReadableDate(timestamp);
    },
    /** @this {Instance} */
    // eslint-disable-next-line complexity, max-statements
    cmdSetRun(runNumber  /*: number*/, approve /*: boolean */ = false) {
      if (!this.client) { return; }
      var run = _.find(this.rfmRuns, { runNumber });
      if (!run) { return; }

      /** @type {Array<any>|any} */
      var params = [];
      const paramMap = this.fromCurrentRun ? CurrentRunParamsMap : RunParamsMap;
      // RunNumber
      params.push({ index: paramMap.runNumber.idx, type: paramMap.runNumber.type,
        value: run.runNumber });

      // Experiment Name
      let refExp = this.$refs[`experiment_${runNumber}`];
      // I do not know why refs returns an array here. It's dark magic.
      if (_.isArray(refExp)) { refExp = _.first(refExp); }
      const editExpVal = refExp.editValue;
      let expIsSet = !_.isEmpty(run.experiment);
      if (run.experiment !== editExpVal) {
        expIsSet = true;
        params.push({ index: paramMap.experiment.idx, type: paramMap.experiment.type,
          value: editExpVal });
      }

      // Expiry Date
      let refExpiry = this.$refs[`expiry_${runNumber}`];
      if (_.isArray(refExpiry)) { refExpiry = _.first(refExpiry); }
      const editExpiry = _.toNumber(refExpiry.editTimestamp);
      if (!_.isNaN(editExpiry) && (run.expiryDate !== editExpiry)) {
        params.push({ index: paramMap.expiryDate.idx, type: paramMap.expiryDate.type,
          value: editExpiry });
      }

      // Approve - We can approve only if there is an experiment name (or is going to be set with this call)
      // TODO use ParamInput Errors/Warnings instead of logger (or maybe both).
      if (approve) {
        if (!expIsSet) {
          logger.error('It is not possible to approve a run without the experiment name.');
          return;
        }
        if (_.isEmpty(editExpVal)) {
          logger.error('It is not possible to set empty experiment name while approving the run.');
          return;
        }
        params.push({ index: paramMap.approved.idx, type: paramMap.approved.type, value: true });
      }

      if (!this.fromCurrentRun) {
        params = { name: "run", index: runNumber, value: params };
      }

      this.client.setParams(params)
      .then(
        () => {}, // success
        this.handleError // fail
      );
    }
  }
});
export default component;
