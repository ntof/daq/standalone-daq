// @ts-check
import _ from 'lodash';
import { UrlUtilitiesMixin } from '../../utilities';
import { DicXmlValue, xml } from '@ntof/redim-client';
import {
  BaseCollapsible as Collapsible,
  BaseLogger as logger } from '@cern/base-vue';
import RFMergerStatsView from './RFMergerStatsView.vue';
import { RFMergerStats } from '../../interfaces/rfmerger';
import Vue from 'vue';
import { mapState } from 'vuex';

/**
 * @typedef {V.Instance<typeof component>
 *  & V.Instance<typeof UrlUtilitiesMixin>} Instance
 */

const component = Vue.extend({
  name: 'RFMergerGlobalStats',
  components: { Collapsible, RFMergerStatsView },
  mixins: [
    UrlUtilitiesMixin
  ],
  props: {
    inEdit: { type: Boolean, default: false }
  },
  /**
   * @return {{
   *   client?: DicXmlValue
   *   rfmInfo: ?RFMergerStats,
   *   infoLoading: boolean }}
   */
  data() {
    return { rfmInfo: null, infoLoading: false };
  },
  computed: {
    ...mapState([ 'dns' ])
  },
  watch: {
    /** @this {Instance} */
    dns() { this.monitor(); }
  },
  /** @this {Instance} */
  mounted() {
    this.monitor();
  },
  /** @this {Instance} */
  beforeDestroy() {
    this.close();
  },
  methods: {
    /** @this {Instance} */
    close() {
      if (this.client) {
        this.client.removeListener('value', this.handleValue);
        this.client.removeListener('error', this.handleError);
        this.client.close();
      }
      this.client = null;
    },
    /** @this {Instance} */
    monitor() {
      this.close();
      // @ts-ignore: data is defined
      _.assign(this, this.$options.data());
      if (_.isEmpty(this.dns)) { return; }
      this.infoLoading = true;

      // MERGER/Info
      this.client = new DicXmlValue('MERGER/Info',
        { proxy: this.getProxy() },  this.dnsUrl(this.dns));
      this.client.on('error', this.handleError);
      this.client.on('value', this.handleValue);
      this.client.promise().catch(this.handleError);
    },
    /**
     * @this {Instance}
     * @param  {any} value
     */
    handleValue(value /*: any */) {
      if (!value) { return; }
      const dataJs = xml.toJs(value);
      this.rfmInfo = _.transform(dataJs, (ret, dataset) => {
        _.forEach(dataset, (data) => {
          // @ts-ignore
          ret[_.get(data, '$.name')] = _.toNumber(_.get(data, '$.value'));
        });
      }, new RFMergerStats());
      this.infoLoading = false;
      this.$emit('stats-updated', this.rfmInfo);
    },
    /**
     * @this {Instance}
     * @param {Error|string} err
     */
    handleError(err/*: any */) {
      logger.error(err);
    }
  }
});
export default component;
