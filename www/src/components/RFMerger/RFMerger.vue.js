// @ts-check
import _ from 'lodash';
import { UrlUtilitiesMixin } from '../../utilities';
import { BaseDimStateAlert as State } from '@ntof/ntof-vue-widgets';
import RFMergerRuns from './RFMergerRuns.vue';
import RFMergerGlobalStats from './RFMergerGlobalStats.vue';
import RFMergerStatsView from './RFMergerStatsView.vue';

import {
  BaseAnimationBlock as AnimBlock,
  BaseCard as Card,
  BaseCollapsible as Collapsible,
  BaseKeyboardEventMixin as KeyboardEventMixin,
  BaseLogger as logger } from '@cern/base-vue';
import { RFMergerState } from '../../interfaces/rfmerger';
import Vue from 'vue';
import { mapState } from 'vuex';

/**
 * @typedef {V.Instance<typeof component>
 *  & V.Instance<typeof UrlUtilitiesMixin>
 *  & V.Instance<ReturnType<KeyboardEventMixin>>} Instance
 */

const component = Vue.extend({
  name: 'RFMerger',
  components: { Card, State, Collapsible, AnimBlock, RFMergerRuns,
    RFMergerGlobalStats, RFMergerStatsView },
  mixins: [
    KeyboardEventMixin({ local: true }),
    UrlUtilitiesMixin
  ],
  /**
   * @return {{ inEdit: boolean, currentRunNumber: ?number, globalTransferred: number, globalRate: number, rfmState: number }}
   */
  data() {
    return { inEdit: false, rfmState: -1,
      currentRunNumber: null, globalTransferred: -1, globalRate: -1 };
  },
  computed: {
    ...mapState([ 'dns' ]),
    .../** @type {{ showKeyHints: boolean }} */mapState('ui', [ 'showKeyHints' ]),
    /**
     * @this {Instance}
     * @return {string}
     */
    stateClass() {
      return (this.rfmState >= RFMergerState.OK) ?
        'alert-success' : 'alert-primary';
    },
    /**
     * @this {Instance}
     * @return {boolean}
     */
    showFooter() {
      // @ts-ignore
      return _.isNumber(parseFloat(this.currentRunNumber)) &&
        this.globalTransferred !== -1 &&
        this.globalRate !== -1;
    },
    /**
     * @this {Instance}
     * @return {boolean}
     */
    isDnsEmpty() {
      return _.isEmpty(this.dns);
    }
  },
  watch: {
    /**
     * @this {Instance}
     * @param {string} newDns
     */
    dns(newDns /*: string */) {
      if (_.isEmpty(newDns)) {
        _.assign(this, this.$options.data());
      }
    }
  },
  /**
   * @this {Instance}
   */
  mounted() {
    _.bindAll(this, [ 'toggleEdit' ]);
    this.onKey('e', this.toggleEdit);
    this.onKey('esc', this.toggleEdit);
  },
  methods: {
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    setEdit(value/*: any */) {
      this.inEdit = value;
    },
    /**
     * @this {Instance}
     */
    toggleEdit() {
      this.setEdit(!this.inEdit);
    },
    /**
     * @this {Instance}
     * @param {{ state: number }?=} state
     */
    onRFMergerStateChange(state/*: any */) {
      this.rfmState = _.get(state, 'state');
    },
    /**
     * @this {Instance}
     * @param {number} currentRunNumber
     */
    updateCurrentRun(currentRunNumber /*: number*/) {
      this.currentRunNumber = currentRunNumber;
    },
    /**
     * @this {Instance}
     * @param {{ transferred: ?number, rate: ?number}} stats
     */
    updateStats(stats /*: RFMergerStats */) {
      this.globalTransferred = stats.transferred;
      this.globalRate = stats.rate;
    },
    /**
     * @this {Instance}
     * @param {Error|string} err
     */
    handleError(err/*: any */) {
      logger.error(err);
    }
  }
});
export default component;
