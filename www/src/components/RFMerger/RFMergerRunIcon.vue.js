// @ts-check

import { every } from 'lodash';
import Vue from 'vue';

/**
 * @typedef {import('../../interfaces/rfmerger').RFMergerRun} RFMergerRun
 */

/** @enum {number} */
const RunState = {
  Running: 0,
  Waiting: 1,
  Archiving: 2
};

/**
 * @typedef {V.Instance<typeof component>} Instance
 */

const component = Vue.extend({
  name: 'RFMergerRunIcon',
  props: {
    run: { type: /** @type {Vue.PropType<RFMergerRun>} */ (Object), default: null }
  },
  computed: {
    /**
     * @this {Instance}
     * @return {boolean}
     */
    isFailed() {
      return this.run && (this.run.failed !== 0);
    },
    /**
     * @this {Instance}
     * @return {boolean}
     */
    hasIgnored() {
      return this.run && (this.run.ignored !== 0);
    },
    /**
     * @this {Instance}
     * @return {RunState}
     */
    state() {
      if (!this.run || !this.run.stopDate || this.run.stopDate <= 0) {
        return RunState.Running;
      }
      else if (!this.run.approved) {
        return RunState.Waiting;
      }
      else {
        return RunState.Archiving;
      }
    },
    /**
     * @this {Instance}
     * @return {boolean}
     */
    isMigrated() {
      return this.run &&
        every([ 'incomplete', 'waitingApproval', 'waiting', 'transferring',
          'copied', 'failed', 'ignored' ], // @ts-ignore
        (s) => (this.run[s] === 0));
    }
  },
  methods: {
    /**
     * @this {Instance}
     * @param  {RunState} state
     * @return {Array<string>}
     */
    // eslint-disable-next-line complexity
    getClass(state /*: typeof RunState */) {
      const k = [];
      let colored = true;
      if (this.isFailed) {
        k.push('text-danger');
      }
      else if (this.hasIgnored) {
        k.push('text-warning');
      }
      else {
        colored = false;
      }

      switch (state) {
      case RunState.Running:
        k.push('fa-angle-double-right');
        if (!colored) {
          k.push('text-primary');
        }
        break;
      case RunState.Waiting: k.push('fa-user-clock'); break;
      case RunState.Archiving:
        k.push('fa-archive');
        if (!colored && !this.isMigrated) {
          k.push('text-primary');
        }
      }
      return k;
    },
    /**
     * @this {Instance}
     * @param  {RunState} state
     * @param  {boolean} isMigrated
     * @return {string}
     */
    getTitle(state /*: typeof RunState */, isMigrated /*: boolean */) {
      switch (state) {
      case RunState.Running: return 'running';
      case RunState.Waiting: return 'waiting for approval';
      case RunState.Archiving: return (isMigrated) ? 'archived' : 'archiving';
      default: return '';
      }
    }
  }
});
export default component;
