// @ts-check
import _ from 'lodash';
import { BaseParamReadonly as ParamReadonly } from '@cern/base-vue';
import { RFMergerStats } from '../../interfaces/rfmerger';
import Vue from 'vue';

/**
 * @typedef {V.Instance<typeof component>} Instance
 */

const component = Vue.extend({
  name: 'RFMergerStatsView',
  components: { ParamReadonly },
  props: {
    stats: { type: RFMergerStats, default: null },
    inEdit: { type: Boolean, default: false }
  },
  methods: {
    /**
     * @this {Instance}
     * @param {number} value
     */
    showStats(value) {
      return !_.isNil(value) && value !== 0;
    }
  }
});
export default component;
