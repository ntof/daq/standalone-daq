// @ts-check

import {
  BaseAnimationBlock as AnimBlock,
  BaseCard as Card,
  BaseKeyboardEventMixin as KeyboardEventMixin,
  BaseParamInput as Param,
  BaseParamReadonly as ParamReadonly,
  BaseLogger as logger } from '@cern/base-vue';

import _ from 'lodash';
import { DicXmlParams } from '@ntof/redim-client';
import { UrlUtilitiesMixin } from '../utilities';
import { ParamType } from '../interfaces';
import { ParamsIdx, TimingMode } from '../interfaces/timing';
import Vue from 'vue';
import { mapState } from 'vuex';

import { AcqState } from '../interfaces/daq';

/**
 * @typedef {{
 *  mainCard: V.Instance<typeof Card>,
 *  period: V.Instance<typeof Param>,
 *  repeat: V.Instance<typeof Param>,
 *  pause: V.Instance<typeof Param>
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<{}, Refs>> &
 *  V.Instance<ReturnType<KeyboardEventMixin>> &
 *  V.Instance<typeof UrlUtilitiesMixin>} Instance
 */

const component = /** @type {V.Constructor<{}, Refs>} */ (Vue).extend({
  name: 'Timing',
  components: { Card, Param, ParamReadonly, AnimBlock },
  mixins: [
    KeyboardEventMixin({ local: true }),
    UrlUtilitiesMixin
  ],
  /**
   * @return {{
   *  xmlParams?: ?DicXmlParams,
   *  inEdit: boolean,
   *  currentMode: ?TimingMode,
   *  triggerPeriod: number,
   *  triggerRepeat: number,
   *  triggerPause: number,
   *  eventNumber: number,
   *  editMode: ?TimingMode,
   *  calibHelpVisible: boolean
   *  availableModes: Array<redim.XmlData.EnumDesc>
   * }}
   */
  data() {
    return {
      inEdit: false,
      currentMode: null,
      triggerPeriod: -1,
      triggerRepeat: -1,
      triggerPause: -1,
      eventNumber: -1,

      editMode: null,
      calibHelpVisible: false,
      availableModes: []
    };
  },
  computed: {
    ...mapState([ 'dns' ]),
    .../** @type {{ showKeyHints: boolean }} */mapState('ui', [ 'showKeyHints' ]),
    .../** @type {{ acqState(): AcqState }} */(mapState('daq', [ 'acqState' ])),
    /**
     * @this {Instance}
     * @return {boolean}
     */
    isRunning() { return this.acqState > AcqState.WAITING_TO_START; },
    /** @return {Array<string>} */
    alertClass() {
      if (!_.isNil(this.currentMode) || _.isEmpty(this.dns)) {
        return [ 'alert-primary' ];
      }
      return [ 'alert-warning' ];
    },
    /** @return {string} */
    defaultString() {
      return _.isEmpty(this.dns) ?
        'Please select a server...' : 'Waiting for service...';
    },
    /** @return {boolean} */
    isCalib() {
      const mode = this.inEdit ? this.editMode : this.currentMode;
      return mode === TimingMode.CALIBRATION;
    },
    /** @return {boolean} */
    showEventCount() {
      return !_.isNil(this.currentMode) && this.currentMode !== TimingMode.DISABLED;
    }
  },
  watch: {
    dns() { this.monitor(); },
    inEdit(value /*: boolean */) {
      if (value) {
        this.editMode = this.currentMode;
      }
    }
  },
  mounted() {
    _.bindAll(this, [ 'toggleEdit', 'submitEdit' ]);
    this.onKey('e', this.toggleEdit);
    this.onKey('esc', this.toggleEdit);
    this.onKey('ctrl-s-keydown', this.submitEdit);
    this.monitor();
  },
  beforeDestroy() {
    this.close();
  },
  methods: {
    /** @param {boolean} value */
    setEdit(value/*: boolean */) {
      this.inEdit = value;
    },
    toggleEdit() {
      this.setEdit(!this.inEdit);
    },
    keepFocus() {
      this.$nextTick(() => {
        this.$refs['mainCard'].$el.focus();
      });
    },
    /** @param {TimingMode} value */
    setMode(value /*: number */) {
      this.editMode = value;
    },
    /** @param {TimingMode} mode */
    getModeName(mode /*: number */) {
      return _.get(_.find(this.availableModes, { value: mode }), 'name');
    },
    close() {
      if (this.xmlParams) {
        this.xmlParams.close();
        this.xmlParams.removeAllListeners();
        this.xmlParams = null;
      }
    },
    /** @this {Instance} */
    monitor() {
      this.close();
      // @ts-ignore
      _.assign(this, this.$options.data()); /* reset state */

      if (_.isEmpty(this.dns)) { return; }
      this.xmlParams = new DicXmlParams('Timing',
        { proxy: this.getProxy() }, this.dnsUrl(this.dns));
      this.xmlParams.promise()
      .catch((/** @type {Error} */ err) => logger.error(err));
      this.xmlParams.on('error', (/** @type {Error} */ err) => logger.error(err));
      this.xmlParams.on('value', (/** @type {redim.XmlData.DataSet} */ value) => {
        if (_.isNil(value)) {
          /* error should have been reported */
          // @ts-ignore
          _.assign(this, this.$options.data()); /* reset state */
          return;
        }
        /** @type {{ [index: number]: redim.XmlData.Data }} */
        var vals = _.transform(value,
          (/** @type {{ [index: number]: redim.XmlData.Data }} */ ret, val) => {
            ret[val.index] = val;
          }, {});

        var mode = vals[ParamsIdx.MODE];
        this.availableModes = _.get(mode, 'enum');
        this.currentMode = _.get(mode, 'value');
        this.eventNumber = _.get(vals, [ ParamsIdx.EVENT_NUMBER, 'value' ]);
        this.triggerPeriod = _.get(vals, [ ParamsIdx.TRIGGER_PERIOD, 'value' ]);
        this.triggerPause = _.get(vals, [ ParamsIdx.TRIGGER_PAUSE, 'value' ]);
        this.triggerRepeat = _.get(vals, [ ParamsIdx.TRIGGER_REPEAT, 'value' ]);
      });
    },
    cancel() {
      this.setEdit(false);
      this.keepFocus();
    },
    /** @param {KeyboardEvent} event */
    // eslint-disable-next-line complexity
    submitEdit(event /*: KeyboardEvent */) {
      if (event) {
        event.preventDefault();
        if (!this.inEdit) { return; } // Keyboard Shortcut Guard
      }
      if (!this.xmlParams) { return; }
      /** @type {redim.XmlData.ParamPart[]} */
      var params = [];
      if (!_.isNil(this.editMode) && this.currentMode !== this.editMode) {
        params.push({ index: ParamsIdx.MODE, type: ParamType.ENUM, value: this.editMode });
      }
      if (this.triggerPeriod !== _.toNumber(this.$refs.period.editValue)) {
        params.push({ index: ParamsIdx.TRIGGER_PERIOD, type: ParamType.INT32,
          value: _.toNumber(this.$refs.period.editValue) });
      }
      if (this.triggerPause !== _.toNumber(this.$refs.pause.editValue)) {
        params.push({ index: ParamsIdx.TRIGGER_PAUSE, type: ParamType.INT32,
          value: _.toNumber(this.$refs.pause.editValue) });
      }
      if (this.triggerRepeat !== _.toNumber(this.$refs.repeat.editValue)) {
        params.push({ index: ParamsIdx.TRIGGER_REPEAT, type: ParamType.INT32,
          value: _.toNumber(this.$refs.repeat.editValue) });
      }
      /*
        event number is not reset on purpose, one can change timing whilst
        acquisition is running
       */
      if (!_.isEmpty(params)) {
        this.xmlParams.setParams(params)
        .then(
          () => { this.setEdit(false); this.keepFocus(); },
          (/** @type {Error} */ err) => logger.error(err)
        );
      }
      else {
        this.setEdit(false);
        this.keepFocus();
      }
    }
  }
});
export default component;
