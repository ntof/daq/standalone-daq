// @ts-check
import _ from 'lodash';
import d from 'debug';
import { DicXmlParams } from '@ntof/redim-client';
import {
  BaseAnimationBlock as AnimBlock,
  BaseCollapsible as Collapsible,
  BaseParamInput as Param,
  BaseParamList as ParamList,
  BaseParamReadonly as ParamReadonly,
  BaseLogger as logger } from '@cern/base-vue';

import ChannelCalibration from './ChannelCalibration.vue';
import { UrlUtilitiesMixin } from '../utilities';
import { ParamType } from '../interfaces';
import { ChannelParamsMap as ParamsMap } from '../interfaces/daq';
import { CardLimits } from '../Consts';
import Vue from 'vue';
import { mapState } from 'vuex';

var debug = d('daq:chan');

/**
 * @typedef {{
 *  threshold: V.Instance<typeof Param>,
 *  zsStart: V.Instance<typeof Param>,
 *  preSamples: V.Instance<typeof Param>,
 *  postSamples: V.Instance<typeof Param>,
 *  lowerLimit: V.Instance<typeof Param>,
 *  offset: V.Instance<typeof Param>,
 *  fullScale: V.Instance<typeof ParamList>,
 *  detectorId: V.Instance<typeof Param>,
 *  detectorType: V.Instance<typeof Param>,
 *  impedance: V.Instance<typeof ParamList>,
 *  thresholdSign: V.Instance<typeof ParamList>
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<object, Refs>> &
 *  V.Instance<typeof UrlUtilitiesMixin>} Instance
 */

const component = /** @type {V.Constructor<object, Refs>} */ (Vue).extend({
  name: 'Channel',
  components: { AnimBlock, Param, ParamList, ParamReadonly, Collapsible, ChannelCalibration },
  mixins: [
    UrlUtilitiesMixin
  ],
  props: {
    card: { type: /** @type {Vue.PropType<App.CardInfo>} */ (Object), default: () => ({
      name: 'unknown', nbChannel: 0, type: '', serialNumber: '' }) },
    channel: { type: Number, default: null },
    inEdit: { type: Boolean, default: false }
  },
  /**
   * @return {{
   *  xmlParams?: ?DicXmlParams,
   *  isLoading: boolean,
   *  enabled: ?boolean,
   *  detectorType: ?string,
   *  detectorId: ?number,
   *  chassisId: ?number,
   *  sampleRate: ?number,
   *  sampleSize: ?number,
   *  fullScale: ?number,
   *  delay: ?number,
   *  threshold: ?number,
   *  thresholdSign: ?number,
   *  zsStart: ?number,
   *  offset: ?number,
   *  preSamples: ?number,
   *  postSamples: ?number,
   *  impedance: ?number,
   *  isZSPEdit: ?boolean,
   *  edit: { [p in keyof ParamsMap]: any }
   * }}
   */
  data() {
    return _.assign({ isLoading: true, isZSPEdit: null },
      _.mapValues(ParamsMap, _.constant(null)),
      { edit: _.mapValues(ParamsMap, _.constant(null)) });
  },
  computed: {
    ...mapState({ dns: 'dns', daq: 'daqName' }),
    /**
     * @this {Instance}
     * @return {number}
     */
    lowerLimit() {
      // @ts-ignore
      return -(this.fullScale / 2) - this.offset;
    },
    /**
     * @this {Instance}
     * @return {Array<{ value: string, text: string }>}
     */
    ImpedanceValues() {
      const arr = _.get(CardLimits, [ this.card.type, 'ImpedanceLimits', 'values' ]);
      return _.map(arr, function(el) { return { value: el, text: el };});
    },
    /**
     * @this {Instance}
     * @return {Array<{value: number, text: string }>}
     */
    FullScaleValues() {
      const arr = _.get(CardLimits, [ this.card.type, 'FullScaleLimits', 'values' ]);
      return _.map(arr, function(el) { return { value: el, text: el }; });
    },
    /**
     * @this {Instance}
     * @return {number}
     */
    OffsetLimits() {
      const fullScaleBase = _.get(CardLimits, [ this.card.type, 'FullScaleBaseValue' ]);
      if (!_.isNil(fullScaleBase)) {
        // @ts-ignore
        if (this.fullScale > fullScaleBase) {
          return _.get(CardLimits, [ this.card.type, 'OffsetLimits', 'higher' ]);
        }
        else {
          return _.get(CardLimits, [ this.card.type, 'OffsetLimits', 'lower' ]);
        }
      }
      return _.get(CardLimits, [ this.card.type, 'OffsetLimits', 'base' ]);
    },
    /**
     * @brief timeWindow in ms
     * @this {Instance}
     * @return {number}
     */
    timeWindow() {
      /* parseFloat will remove trailing zeroes */
      return ((this.sampleSize || 0) / (this.sampleRate || 1));
    },
    /** @return {boolean} */
    isZSP() { return (this.zsStart || 0) < (this.timeWindow * 1e6); }
  },
  watch: {
    inEdit() {
      this.isZSPEdit = this.isZSP;
    },
    isZSP() {
      /* configure may be called when not in edit mode, better update this */
      if (!this.inEdit) {
        this.isZSPEdit = this.isZSP;
      }
    }
  },
  /** @this {Instance} */
  mounted() { this.monitor(); },
  /** @this {Instance} */
  beforeDestroy() { this.close(); },
  methods: {
    /** @this {Instance} */
    close() {
      if (this.xmlParams) {
        this.xmlParams.close();
        this.xmlParams.removeAllListeners();
        this.xmlParams = null;
      }
    },
    /** @this {Instance} */
    updateEditOffset() {
      this.$refs.offset.editValue =
        -(_.toNumber(this.$refs.fullScale.editValue) / 2) -
        _.toNumber(this.$refs.lowerLimit.editValue);
      this.$refs.fullScale.checkValidity();
    },
    /** @this {Instance} */
    monitor() {
      this.close();
      // @ts-ignore
      _.assign(this, this.$options.data()); /* reset state */
      this.xmlParams = new DicXmlParams(`${this.daq}/${this.card.name}/CHANNEL${this.channel}`,
        { proxy: this.getProxy() }, this.dnsUrl(this.dns));

      this.xmlParams.promise()
      .catch((err) => logger.error(err));
      this.xmlParams.on('value', (value) => {
        var vals = _.transform(value, (ret, val) => {
          ret[val.index] = val;
        }, []);

        _.forEach(ParamsMap, (param, name) => {
          this[name] = _.get(vals, [ param.idx, 'value' ], null);
        });

        this.isLoading = false;
        this.$emit('loaded', this.$data, this.channel);
        debug('channel update', this.getKey());
      });
    },
    /**
     * @this {Instance}
     * @param  {?string=} value
     * @return {string}
     */
    getKey(value /*: any */) {
      return this.card.name + '_CHAN' + this.channel + ((value) ? ('_' + value) : '');
    },
    /** @this {Instance} */
    checkOffsetWarning() {
      this.$refs.offset.removeWarning('exceeded');
      const offsetLimits = this.OffsetLimits;
      if (_.toNumber(this.$refs.offset.editValue) > offsetLimits || _.toNumber(this.$refs.offset.editValue) < -offsetLimits) {
        this.$refs.offset.addWarning('exceeded', 'offset should be below ' + offsetLimits + ' and above ' + (-offsetLimits));
      }
    },
    /**
     * @this {Instance}
     * @param  {any} extraValues
     */
    configure(extraValues /*: any */) {
      if (!this.isZSPEdit) {
        const sampleSize = _.get(extraValues, [ 'sampleSize' ], this.sampleSize);
        const sampleRate = _.get(extraValues, [ 'sampleRate' ], this.sampleRate);

        this.edit.zsStart = _.toString((sampleSize || 0) / (sampleRate || 1) * 1e6);
      }

      var params = [];
      _.forEach(ParamsMap, (desc, name) => {
        var edit = _.has(extraValues, name) ?
          extraValues[name] : _.get(this, [ 'edit', name ]);

        if (!_.isNil(edit)) {
          if (desc.type !== ParamType.STRING) {
            edit = _.toNumber(edit);
          }
          params.push({ value: edit, index: desc.idx, type: desc.type });
        }
      });

      if (!_.isEmpty(params) && this.xmlParams) {
        debug('configuring channel', this.getKey(), params);
        return this.xmlParams.setParams(params);
      }
      return Promise.resolve();
    },
    onZSPEdit(value) {
      this.isZSPEdit = value;
      if (!this.inEdit) { return; }
      this.$refs.zsStart.editValue = _.toString((this.isZSPEdit) ?
        0 : (this.timeWindow * 1e6));
    }
  }
});
export default component;
