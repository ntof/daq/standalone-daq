// @ts-check
import { compact, get, isEmpty, keyBy, keys, map, omit, sortBy } from 'lodash';
import * as Consts from '../Consts';
import { DicDns } from '@ntof/redim-client';
import { UrlUtilitiesMixin } from '../utilities';
import { BaseLogger as logger } from '@cern/base-vue';
import Vue from 'vue';
import { mapState } from 'vuex';

/**
 * @typedef {{
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<typeof options, Refs>> &
 *  V.Instance<typeof UrlUtilitiesMixin>} Instance
 */

const options = {
  areaMap: keyBy(Consts.AreaList, 'dns')
};

const DaqNameRe = new RegExp('ntofdaq-m([0-9]{1,}).*');

const component = /** @type {V.Constructor<typeof options, Refs>} */ (Vue).extend({
  name: 'SidebarContent',
  ...options,
  mixins: [
    UrlUtilitiesMixin
  ],
  /**
   * @return {{
   *  dnsInput: string,
   *  daqList: Array<string>
   * }}
   */
  data() { return { dnsInput: '', daqList: [] }; },
  computed: {
    ...mapState({ dns: 'dns', daq: 'daqName' }),
    /** @return {string} */
    serverName() {
      if (this.dnsInput === '') {
        return "Select server";
      }
      return get(this.$options.areaMap, [ this.dnsInput, 'name' ], this.dnsInput);
    }
  },
  watch: {
    /** @param {string} value */
    dns(value) {
      this.dnsInput = value;
      this.listDaqs();
    }
  },
  mounted() {
    this.dnsInput = this.dns;
    this.listDaqs();
  },
  methods: {
    /** @param {?string=} value */
    selectDns(value) {
      value = value || this.dnsInput;
      /* also remove daq */
      var query = omit(this.$route.query, [ 'dns', 'daq' ]);
      if (!isEmpty(value)) {
        query.dns = value;
      }
      this.$router.push({ query }).catch(() => {});
    },
    /** @param {string} value */
    selectDaq(value) {
      var query = omit(this.$route.query, [ 'daq' ]);
      if (!isEmpty(value)) {
        query.daq = value;
      }
      const prom = this.$router.push({ query });
      if (prom) { prom.catch(() => {}); }
      this.$emit('toggle-menu');
    },
    /** @this {Instance} */
    async listDaqs() {
      this.daqList = [];
      if (isEmpty(this.dns)) {
        return;
      }
      const rep = await DicDns.serviceInfo('*/ListDaqElements',
        { proxy: this.getProxy() }, this.dnsUrl(this.dns))
      .catch((err) => logger.error(err));

      if (rep) {
        var daqs = keys(rep);
        this.daqList = sortBy(compact(
          map(daqs, (d) => d.slice(0, d.indexOf('/ListDaqElements')))));
      }
    },
    /**
     * @param {string} daqUrl
     */
    getDaqName(daqUrl) {
      var match = DaqNameRe.exec(daqUrl);
      return (match) ? ('DAQ ' + match[1]) : daqUrl;
    }
  }
});
export default component;
