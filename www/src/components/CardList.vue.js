// @ts-check
import _ from 'lodash';
import { DicXmlValue } from '@ntof/redim-client';
import { UrlUtilitiesMixin } from '../utilities';
import ZeroSuppression from './AppZeroSuppression.vue';
import DaqCard from './Card.vue';
import {
  BaseKeyboardEventMixin as KeyboardEventMixin,
  BaseLogger as logger } from '@cern/base-vue';
import DaqManagerCmdMixin from '../mixins/DaqManagerCmdMixin';
import { AcqState, DaqState } from '../interfaces/daq';
import WaitForMixin from './BaseWaitForMixin';

import Vue from 'vue';
import { mapState } from 'vuex';

/**
 * @typedef {import('@cern/base-vue').BaseCard} BaseCard
 * @typedef {{
 *  mainCard: V.Instance<BaseCard>,
 *  zsp: V.Instance<typeof ZeroSuppression>,
 *  Cards: Array<V.Instance<typeof DaqCard>>
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<object, Refs>> &
 *  V.Instance<typeof WaitForMixin> &
 *  V.Instance<ReturnType<KeyboardEventMixin>> &
 *  V.Instance<typeof UrlUtilitiesMixin> &
 *  V.Instance<typeof DaqManagerCmdMixin>} Instance
 */

const component = /** @type {V.Constructor<object, Refs>} */ (Vue).extend({
  name: 'CardList',
  components: { DaqCard, ZeroSuppression },
  mixins: [
    WaitForMixin,
    KeyboardEventMixin({ local: true }),
    UrlUtilitiesMixin,
    DaqManagerCmdMixin
  ],
  /**
   * @return {{
   *  cards: ?{ [name: string]: App.CardInfo },
   *  isConfiguring: boolean,
   *  cardsLoading: boolean,
   *  zspLoading: boolean,
   *  inEdit: boolean
   * }}
   */
  data() {
    return { cards: null, cardsLoading: false, zspLoading: false, isConfiguring: false, inEdit: false };
  },
  computed: {
    ...mapState({ dns: 'dns', daq: 'daqName' }),
    .../** @type {{ showKeyHints: boolean }} */mapState('ui', [ 'showKeyHints' ]),
    /**
     * @this {Instance}
     * @return {boolean}
     */
    isLoading() { return this.cardsLoading || this.zspLoading; },
    .../** @type {{ daqState(): DaqState, acqState(): AcqState }} */(mapState('daq', [ 'daqState', 'acqState' ])),
    /**
     * @this {Instance}
     * @return {boolean}
     */
    canConfigure() {
      return !this.isLoading &&
        (this.acqState === AcqState.IDLE) &&
        ((this.daqState === DaqState.NOT_CONFIGURED) ||
        (this.daqState === DaqState.WAITING_FOR_COMMAND));
    }
  },
  watch: {
    /** @this {Instance} */
    daq() { this.listCards(); }
  },
  /** @this {Instance} */
  mounted() {
    _.bindAll(this, [ 'toggleEdit', 'configure' ]);
    this.onKey('e', this.toggleEdit);
    this.onKey('esc', this.toggleEdit);
    this.onKey('ctrl-s-keydown', this.configure);
    this.listCards();
  },
  methods: {
    /**
     * @this {Instance}
     * @param {boolean} value
     */
    setEdit(value/*: any */) {
      this.inEdit = value;
    },
    /** @this {Instance} */
    toggleEdit() {
      this.setEdit(!this.inEdit);
    },
    /** @this {Instance} */
    keepFocus() {
      this.$nextTick(() => {
        // @ts-ignore
        this.$refs['mainCard'].$el.focus();
      });
    },
    /** @this {Instance} */
    cancel() {
      this.setEdit(false);
      this.keepFocus();
    },
    /**
     * @this {Instance}
     * @param {KeyboardEvent}  event
     * @param {boolean} [keepFocus=true]
     */ // eslint-disable-next-line complexity
    async configure(event, keepFocus = true) {
      if (event) {
        event.preventDefault();
        if (!this.inEdit || this.isLoading || !this.canConfigure) { return; } // Keyboard Shortcut Guard
      }
      this.isConfiguring = true;
      try {
        const calibRequired = (this.daqState !== DaqState.NOT_CONFIGURED);

        for (const card of this.$refs.Cards) {
          await card.configure();
        }
        await this.$refs.zsp.configure();
        if (calibRequired) {
          await this.waitFor('daqState', DaqState.WAITING_FOR_COMMAND, 10000);
          await this.daqCalibrate(this.daq, this.dns);
        }
        this.setEdit(false);
        if (keepFocus) {
          this.keepFocus();
        }
      }
      catch (err) {
        logger.error(err);
      }
      this.isConfiguring = false;
    },
    /**
     * @this {Instance}
     * @param  {string} cardName
     */
    getKey(cardName) {
      return `${this.dns}-${this.daq}-${cardName}`;
    },
    /** @this {Instance} */
    listCards() {
      // @ts-ignore
      _.assign(this, this.$options.data());

      if (!_.isEmpty(this.daq) && !_.isEmpty(this.dns)) {
        this.cardsLoading = true;
        this.zspLoading = _.get(this, '$refs.zsp.isLoading', false);

        DicXmlValue.get(this.daq + '/ListDaqElements',
          { proxy: this.getProxy() },  this.dnsUrl(this.dns))
        .then(
          (value) => {
            // @ts-ignore we can iterate on Element collection
            this.cards = _.transform(_.get(value, 'children'),
              function(/** @type {{ [name: string]: App.CardInfo }} */ret, elt) {
                const name = _.toUpper(elt.tagName);
                if (!_.startsWith(name, 'CARD')) { return; }

                ret[name] = {
                  name: name,
                  nbChannel: _.toNumber(elt.getAttribute('nbChannel')),
                  type: elt.getAttribute('type'),
                  serialNumber: elt.getAttribute('serialNumber')
                };
              }, /** @type {{ [name: string]: App.CardInfo }} */ {});

            if (_.isEmpty(this.cards)) {
              this.onCardLoaded();
            }
          },
          (err) => logger.error(err)
        );
      }
    },
    /** @this {Instance} */
    onCardLoaded() {
      if (_.every(this.$refs.Cards, { isLoading: false })) {
        this.$refs.zsp.updateChannels(this.$refs.Cards);
        this.cardsLoading = false;
        if (!this.zspLoading) {
          this.$emit('loaded');
        }
      }
    },
    /**
     * @this {Instance}
     * @param  {boolean} value
     */
    onZSPLoad(value) {
      this.zspLoading = value;
      if (!value && !this.cardsLoading) {
        this.$emit('loaded');
      }
    }
  }
});
export default component;
