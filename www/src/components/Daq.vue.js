// @ts-check
/* eslint-disable max-lines */
import _ from 'lodash';

import { BaseCard as Card,
  BaseKeyboardEventMixin as KeyboardEventMixin,
  BaseParamInput as Param,
  BaseLogger as logger } from '@cern/base-vue';
import { BaseDimStateAlert as State } from '@ntof/ntof-vue-widgets';
import { DicXmlCmd, DicXmlValue } from '@ntof/redim-client';

import { UrlUtilitiesMixin, tap } from '../utilities';
import { ParamType } from '../interfaces';
import { TimingMode, ParamsIdx as TimingParamsIdx } from '../interfaces/timing';
import { AcqState, DaqState } from '../interfaces/daq';

import RunNumberParam from './AppRunNumberParam.vue';
import WaitForMixin from './BaseWaitForMixin';
import RFMergerCmdMixin from '../mixins/RFMergerCmdMixin';
import DaqManagerCmdMixin from '../mixins/DaqManagerCmdMixin';

import Vue from 'vue';
import { mapState } from 'vuex';

/**
 * @typedef {V.Instance<typeof component> &
 *  V.Instance<typeof UrlUtilitiesMixin> &
 *  V.Instance<typeof WaitForMixin> &
 *  V.Instance<ReturnType<KeyboardEventMixin>> &
 *  V.Instance<typeof RFMergerCmdMixin> &
 *  V.Instance<typeof DaqManagerCmdMixin>} Instance
 */

const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: 'Daq',
  components: { Card, State, Param, RunNumberParam },
  mixins: [
    WaitForMixin,
    KeyboardEventMixin({ local: true }),
    UrlUtilitiesMixin,
    RFMergerCmdMixin,
    DaqManagerCmdMixin
  ],
  props: {
    daqId: { type: Number, default: null },
    canConfigure: { type: Boolean }
  },
  /**
   * @return {{
   *  client?: ?DicXmlValue,
   *  runNumber: ?number,
   *  eventNumber: ?number,
   *  sizeNumber: ?number,
   *  inEdit: boolean,
   *  isSendingCommand: boolean,
   *  nextRunNumber: ?number
   * }}
   */
  data() {
    return {
      runNumber: null, eventNumber: null, sizeNumber: null, inEdit: false,
      isSendingCommand: false,
      nextRunNumber: null
    };
  },
  computed: {
    ...mapState({ dns: 'dns', daq: 'daqName' }),
    .../** @type {{ showKeyHints: boolean }} */mapState('ui', [ 'showKeyHints' ]),
    .../** @type {{ daqState(): DaqState, acqState(): AcqState }} */(mapState('daq', [ 'daqState', 'acqState' ])),
    /**
     * @this {Instance}
     * @return {boolean}
     */
    isRunning() {
      // don't set this when daq is in ERROR (-2)
      return (this.acqState > AcqState.WAITING_TO_START) &&
        (this.daqState >= DaqState.WAITING_FOR_COMMAND);
    },
    /**
     * @this {Instance}
     * @return {string}
     */
    stateClass() {
      return (this.acqState > AcqState.WAITING_TO_START) ?
        'alert-success' : 'alert-primary';
    }
  },
  watch: {
    daq() { this.monitor(); },
    dns() { this.monitor(); }
  },
  /** @this {Instance} */
  mounted() {
    _.bindAll(this, [ 'toggleEdit', 'cmdConfigure',
      'cmdStart', 'cmdStop', 'cmdReset' ]);
    this.onKey('e', this.toggleEdit);
    this.onKey('esc', this.toggleEdit);
    this.onKey('c', this.cmdConfigure);
    this.onKey('enter', this.cmdStart);
    this.onKey('x', this.cmdStop);
    this.onKey('r', this.cmdReset);
    this.monitor();
  },
  beforeDestroy() {
    this.close();
  },
  methods: {
    /**
     * @param {boolean} value
     */
    setEdit(value) {
      this.inEdit = value;
    },
    toggleEdit() {
      this.setEdit(!this.inEdit);
    },
    keepFocus() {
      this.$nextTick(() => {
        if (this.$refs['mainCard']) {
          this.$refs['mainCard'].$el.focus();
        }
      });
    },
    /** @param {string} name */
    getService(name) {
      if (!_.isEmpty(this.daq)) {
        return this.daq + '/' + name;
      }
    },
    close() {
      if (this.client) {
        this.client.removeListener('value', this.handleValue);
        this.client.removeListener('error', this.handleError);
        this.client.close();
      }
      this.client = null;
    },
    /** @param {?Element} value */
    handleValue(value) {
      if (_.isNil(value)) {
        this.runNumber = null;
        this.eventNumber = null;
        this.sizeNumber = null;
      }
      else {
        this.runNumber = _.toNumber(_.invoke(value, 'getAttribute', 'run'));
        this.eventNumber = _.toNumber(_.invoke(value, 'getAttribute', 'event'));
        this.sizeNumber = _.toNumber(_.invoke(value, 'getAttribute', 'size'));
      }
    },
    /** @param {?Error} err */
    handleError(err) {
      logger.error(err);
    },
    /** @this {Instance} */
    monitor() {
      this.close();
      // @ts-ignore
      _.assign(this, this.$options.data());

      if (!_.isEmpty(this.daq) && !_.isEmpty(this.dns)) {
        this.client = new DicXmlValue(this.daq + '/WRITER/RunExtensionNumber',
          { proxy: this.getProxy() },  this.dnsUrl(this.dns));
        this.client.on('error', this.handleError);
        this.client.on('value', this.handleValue);

        this.client.promise()
        .catch(this.handleError);
      }
    },
    /**
     * @this {Instance}
     * @param {KeyboardEvent} event
     */
    cmdReset(event) {
      if (event) {
        if (!this.inEdit || this.isRunning) { return; } // Keyboard Shortcut Guard
      }
      if (this.isSendingCommand) { return; }
      this.isSendingCommand = true;
      return this.daqReset(this.daq, this.dns)
      .catch((err) => logger.error(err))
      .finally(() => {
        this.isSendingCommand = false;
        this.keepFocus();
      });
    },
    /**
     * @this {Instance}
     * @param {KeyboardEvent} event
     */
    // eslint-disable-next-line complexity
    async cmdStart(event) {
      if (event) {
        if (!this.inEdit) { return; } // Keyboard Shortcut Guard
        if (this.isRunning || this.daqState !== DaqState.WAITING_FOR_COMMAND || _.isNil(this.daqId)) { return; }
      }
      if (this.isSendingCommand || !this.nextRunNumber || this.nextRunNumber <= 0) { return; }
      const dnsInfo = this.dnsUrl(this.dns);
      const opts = { proxy: this.getProxy() };
      this.isSendingCommand = true;
      try {
        await DicXmlCmd.invoke(this.daq + '/Daq/Command',
          { command: { command: 'initialization?runnumber=' + this.nextRunNumber } },
          opts, dnsInfo);
        await this.waitFor('daqState', DaqState.WAITING_FOR_START_ACQ, 30000);
        const mode = await this.timingSuspend();
        await this.rfmStart(this.nextRunNumber || -1, null, false, true, [ this.daqId ], this.dns);
        await this.daqStart(this.daq, this.dns);
        await this.waitFor('acqState', AcqState.WAITING_FOR_TRIGGER, 10000);
        await this.timingStart(mode);
      }
      catch (err) { logger.error(err); }
      finally {
        this.isSendingCommand = false;
        this.keepFocus();
      }
    },
    /**
     * @this {Instance}
     * @return {Promise<number>}
     */
    timingSuspend() {
      const dnsInfo = this.dnsUrl(this.dns);
      const opts = { proxy: this.getProxy() };

      return DicXmlValue.get('Timing/Aqn', opts, dnsInfo)
      .then((conf) => {
        if (conf) {
          const modeIdx = _.toString(TimingParamsIdx.MODE);
          const data = conf.getElementsByTagName('data');
          for (var i = 0; i < data.length; ++i) {
            var node = data.item(i);
            if (node && node.getAttribute('index') === modeIdx) {
              return _.toNumber(node.getAttribute('value'));
            }
          }
        }
        logger.warning('timing: failed to retrieve current mode');
        return TimingMode.DISABLED;
      })
      /* disable timing */
      .then(tap(() => DicXmlCmd.invoke('Timing', { parameters: { data:
        { '$': { index: TimingParamsIdx.MODE, type: ParamType.ENUM, value: TimingMode.DISABLED } }
      } }, opts, dnsInfo)))
      .catch(() => {
        logger.warning('timing: failed to suspend service');
        return TimingMode.DISABLED;
      });
    },
    /**
     * @this {Instance}
     * @param {number} mode
     */
    timingStart(mode) {
      if (!_.isNumber(mode) || mode <= 0) { return; }

      return DicXmlCmd.invoke('Timing', { parameters: { data: [
        { '$': { index: TimingParamsIdx.EVENT_NUMBER, type: ParamType.INT64, value: 0 } },
        { '$': { index: TimingParamsIdx.MODE, type: ParamType.ENUM, value: mode } }
      ] } }, { proxy: this.getProxy() }, this.dnsUrl(this.dns));
    },
    /**
     * @this {Instance}
     * @param {KeyboardEvent} event
     */
    // eslint-disable-next-line complexity
    async cmdStop(event) {
      if (event) {
        if (!this.inEdit || !this.isRunning) { return; }
      }
      if (this.isSendingCommand) { return; }
      this.isSendingCommand = true;
      try {
        await this.daqStop(this.daq, this.dns);
        // handle case where run hasn't ticked yet
        return this.rfmStop((this.runNumber && this.runNumber > 0) ?
          this.runNumber : (this.nextRunNumber || -1), this.dns)
        .catch((/** @type {Error} */ err) => logger.warning(err)); // rfmStop is a warning
      }
      catch (err) { logger.error(err); }
      finally {
        this.isSendingCommand = false;
        this.keepFocus();
      }
    },
    /** @param {KeyboardEvent} event */
    cmdConfigure(event) {
      if (event) {
        if (!this.inEdit || this.isRunning ||
            this.daqState !== DaqState.NOT_CONFIGURED) { return; }
      }
      if (this.isSendingCommand || !this.canConfigure) { return; }
      this.$emit('configure');
      this.keepFocus();
    },
    /** @param {number|string} value */
    setNextRunNumber(value) {
      this.nextRunNumber = _.toInteger(value);
    }
  }
});
export default component;
