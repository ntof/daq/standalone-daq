
export = App
export as namespace App

declare namespace App {
  interface CardInfo {
    name: string,
    nbChannel: number,
    type: string,
    serialNumber: string
  }

  namespace ZSP {
    interface Channel {
      id: string,
      sn: string,
      channel: number
    }
  }
}
