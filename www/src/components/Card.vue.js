// @ts-check
import _ from 'lodash';
import Channel from './Channel.vue';
import {
  BaseAnimationBlock as AnimBlock,
  BaseCollapsible as Collapsible,
  BaseParamInput as Param,
  BaseParamList as ParamList,
  BaseParamReadonly as ParamReadonly } from '@cern/base-vue';
import { CardLimits } from '../Consts';
import Vue from 'vue';
import { mapState } from 'vuex';

/**
 * @typedef {{ Channels: Array<V.Instance<typeof Channel, Vue> > }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<object, Refs>>} Instance
 */

const component = /** @type {V.Constructor<object, Refs>} */ (Vue).extend({
  name: 'Card',
  components: { Channel, Param, ParamList, ParamReadonly, Collapsible, AnimBlock },
  props: {
    card: { type: /** @type {Vue.PropType<App.CardInfo>} */ (Object), default: null },
    inEdit: { type: Boolean, default: false }
  },
  /**
   * @return {{
   *    sampleRate: ?number,
   *    sampleSize: ?number,
   *    delay: ?number,
   *    maxEditTimeWindow: ?number,
   *    isLoading: boolean
   * }}
   */
  data() {
    return {
      sampleRate: null,
      sampleSize: null,
      delay: null,
      maxEditTimeWindow: 0,
      isLoading: true
    };
  },
  computed: {
    ...mapState({ dns: 'dns', daq: 'daqName' }),
    /**
     * @this {Instance}
     * @return {string}
     */
    cardId() {
      return _.get(this.card, 'name', 'invalid');
    },
    /**
     * @this {Instance}
     * @return {number}
     */
    timeWindow() {
      /* parseFloat will remove trailing zeroes */
      return parseFloat(((this.sampleSize || 0) / (this.sampleRate || 1)).toFixed(3));
    },
    /**
     * @this {Instance}
     * @return {Array<{ value: number, text: number }>}
     */
    SampleRateValues() {
      const arr = _.get(CardLimits, [ this.card.type, 'SampleRateLimits', 'values' ]);
      return _.map(arr, function(el) { return { value: el, text: el }; });
    },
    /**
     * @this {Instance}
     * @return {number|undefined}
     */
    minSampleSize() {
      return _.get(CardLimits, [ this.card.type, 'SampleSizeLimits', 'min' ]);
    },
    /**
     * @this {Instance}
     * @return {number|undefined}
     */
    maxSampleSize() {
      return _.get(CardLimits, [ this.card.type, 'SampleSizeLimits', 'max' ]);
    }
  },
  methods: {
    /**
     * @this {Instance}
     * @param  {string} value
     * @return {string}
     */
    getId(value /*: string */) {
      return this.cardId + '_' + value;
    },
    /**
     * @this {Instance}
     * @param  {{ [chanParam: string]: any}} data
     */
    onChannelLoaded(data /*: { [$Keys<typeof ChannelParamsMap>]: any } */) {
      this.sampleRate = data.sampleRate;
      this.sampleSize = data.sampleSize;
      this.delay = data.delay;
      if (_.every(this.$refs.Channels, { 'isLoading': false })) {
        this.isLoading = false;
        this.$emit('loaded');
      }
    },
    /**
     * @this {Instance}
     */
    updateEditSampleSize() {
      this.$refs.sampleSize.editValue = Math.ceil(
        _.toNumber(this.$refs.timeWindow.editValue) *
        _.toNumber(this.$refs.sampleRate.editValue));
      const sampleRate = _.toNumber(this.$refs.sampleRate.editValue);
      this.maxEditTimeWindow = (sampleRate !== 0) ?
        parseFloat(((this.maxSampleSize || 0) / sampleRate).toFixed(3)) : 0;

      this.$refs.sampleRate.checkValidity();
      this.$refs.timeWindow.checkValidity();
    },
    /**
     * @this {Instance}
     */
    async configure() {
      var params = {};
      var editValue = _.toNumber(this.$refs.sampleRate.editValue);
      params['sampleRate'] = editValue;

      editValue = _.toNumber(this.$refs.sampleSize.editValue);
      params['sampleSize'] = editValue;

      editValue = _.toNumber(this.$refs.delay.editValue);
      params['delay'] = editValue;

      for (const chan of this.$refs.Channels) {
        await chan.configure(params);
      }
    }
  }
});

export default component;
