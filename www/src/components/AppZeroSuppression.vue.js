// @ts-check
import _ from 'lodash';
import { DicXmlParams } from '@ntof/redim-client';
import { UrlUtilitiesMixin } from '../utilities';
import { ZSPConfig } from '../ZeroSuppressionConfig';

import {
  BaseAnimationBlock as AnimBlock,
  BaseCollapsible as Collapsible,
  BaseParamInput as Param,
  BaseParamList as ParamList,
  BaseLogger as logger } from '@cern/base-vue';

import d from 'debug';
import Vue from 'vue';
import { mapState } from 'vuex';

var debug = d('daq:zsp');

/**
 * @typedef {import('./Channel.vue')['default']} Channel
 * @typedef {import('./Card.vue')['default']} Card
 *
 * @typedef {{
 *  detectorType: V.Instance<typeof BaseParamInput>
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<typeof options, Refs>> &
 *  V.Instance<typeof UrlUtilitiesMixin>} Instance
 */

const options = {
  modes: [
    { value: ZSPConfig.MODE.INDEPENDENT, text: 'Independent' },
    { value: ZSPConfig.MODE.SINGLE_MASTER, text: 'Single Master' },
    { value: ZSPConfig.MODE.MASTER, text: 'Multi Master' }
  ]
};

const component = /** @type {V.Constructor<typeof options, Refs>} */ (Vue).extend({
  name: 'AppZeroSuppression',
  components: { Collapsible, ParamList, Param, AnimBlock },
  mixins: [
    UrlUtilitiesMixin
  ],
  props: {
    inEdit: { type: Boolean, default: false }
  },
  /**
   * @return {{
   *  client?: ?DicXmlParams,
   *  isLoading: boolean,
   *  zsp: ?ZSPConfig,
   *  zspEdit: ?ZSPConfig,
   *  channels: ?{ [uuid: string]: V.Instance<Channel> },
   *  ZSPMode: typeof ZSPConfig.MODE
   * }}
   */
  data() {
    return {
      isLoading: false, zsp: null, zspEdit: null,
      channels: null, ZSPMode: ZSPConfig.MODE
    };
  },
  computed: {
    ...mapState({ dns: 'dns', daq: 'daqName' }),
    /**
     * @this {Instance}
     * @return {?ZSPConfig}
     */
    activeZsp() {
      return this.inEdit ? this.zspEdit : this.zsp;
    },
    /**
     * @this {Instance}
     * @return {?string}
     */
    firstMaster() {
      return _.get(this.inEdit ? this.zspEdit : this.zsp, [ 'master', 0, 'id' ]);
    },
    /**
     * @this {Instance}
     * @return {any}
     */
    masterChoiceList() {
      var ret = [ { value: '', text: '' } ];
      // @ts-ignore
      _.transform(_.get(this.inEdit ? this.zspEdit : this.zsp, 'master'),
        (ret, m) => ret.push(this.getChanOpt(m)), ret);
      // @ts-ignore
      _.transform(_.get(this.inEdit ? this.zspEdit : this.zsp, 'indep'),
        (ret, m) => ret.push(this.getChanOpt(m)), ret);
      return ret;
    }
  },
  watch: {
    dns() { this.monitor(); },
    daq() { this.monitor(); },
    inEdit(value) {
      if (value && this.zsp) {
        this.zspEdit = this.zsp.clone();
      }
    },
    channels() {
      if (this.zsp) {
        this.zsp.updateIndep(this.channels);
      }
      if (this.zspEdit) {
        this.zspEdit.updateIndep(this.channels);
      }
    },
    isLoading() {
      this.$emit('loading-change', this.isLoading);
    }
  },
  mounted() {
    this.monitor();
  },
  methods: {
    /**
     * @this {Instance}
     * @param  {Array<V.Instance<Card>>} cards
     */
    updateChannels(cards) {
      this.channels = _.transform(cards,
        (/** @type {{ [uuid: string]: V.Instance<Channel> }} */ ret, card) => {
          _.forEach(card.$refs.Channels, (chan) => {
            ret[card.card.serialNumber + ':' + chan.channel] = chan;
          });
        }, /** @type {{ [uuid: string]: V.Instance<Channel> }} */ {});
    },
    /**
     * @this {Instance}
     * @param  {App.ZSP.Channel} zsp
     */
    getChanName(zsp) {
      var chan = _.get(this.channels, zsp.id);
      if (!chan) {
        return (zsp.sn + ':' + zsp.channel);
      }
      return this.inEdit ?
        chan.$refs.detectorType.editValue : chan.detectorType;
    },
    /**
     * @this {Instance}
     * @param  {App.ZSP.Channel} zsp
     */
    getChanInfo(zsp) {
      var chan = _.get(this.channels, zsp.id);
      return chan ? `[${chan.card.name.substr(4)}:${chan.channel}]` : '';
    },
    /**
     * @this {Instance}
     * @param  {App.ZSP.Channel} zsp
     */
    getChanOpt(zsp) {
      return { value: zsp.id, text: `${this.getChanName(zsp)} ${this.getChanInfo(zsp)}` };
    },
    /**
     * @this {Instance}
     * @param {number} mode
     */
    getModeText(mode) {
      return _.get(_.find(this.$options.modes, { value: mode }), 'text', mode);
    },
    /**
     * @this {Instance}
     * @param  {App.ZSP.Channel} filter
     */
    filteredMasterChoice(filter) {
      return _.filter(this.masterChoiceList,
        (opt) => (opt.value !== filter.id));
    },
    /** @this {Instance} */
    configure() {
      debug('configuring zsp');
      if (!this.zspEdit) {
        /* can be called directly from Daq component, whilst not in edit mode */
        this.zspEdit = this.zsp.clone();
      }

      // @ts-ignore
      return this.client.setParams(this.zspEdit.toParams())
      .finally(() => { this.isSendingCommand = false; });
    },
    /** @this {Instance} */
    close() {
      if (this.client) {
        this.client.removeAllListeners('value');
        this.client.removeAllListeners('error');
        this.client.close();
      }
      this.client = null;
    },
    /** @this {Instance} */
    monitor() {
      this.close();
      // @ts-ignore
      _.assign(this, this.$options.data());

      if (!_.isEmpty(this.daq) && !_.isEmpty(this.dns)) {
        this.isLoading = true;
        this.client = new DicXmlParams(this.daq + '/ZeroSuppression',
          { proxy: this.getProxy() },  this.dnsUrl(this.dns));
        this.client.on('error', (err) => logger.error(err));
        this.client.on('value', (value) => {
          this.zsp = ZSPConfig.fromParam(value);
          if (!this.zsp) {
            logger.error('failed to load ZeroSuppression configuration');
            return;
          }
          this.zsp.updateIndep(this.channels);
          debug('zsp update');

          this.isLoading = false;
          this.$emit('loaded');
        });
      }
    }
  }
});
// @ts-ignore
_.assign(component.options, options);
export default component;
