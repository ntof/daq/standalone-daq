// @ts-check

import { assign, constant, forEach, get, mapValues, transform } from 'lodash';
import d from 'debug';
import { DicXmlDataSet } from '@ntof/redim-client';
import {
  BaseParamReadonly as ParamReadonly,
  BaseLogger as logger } from '@cern/base-vue';
import { UrlUtilitiesMixin } from '../utilities';
import { ChannelCalibrationMap } from '../interfaces/daq';
import { CardInfo } from '../Consts';
import Vue from 'vue';
import { mapState } from 'vuex';

var debug = d('daq:chan');

/**
 * @typedef {V.Instance<typeof component> &
 *  V.Instance<typeof UrlUtilitiesMixin>} Instance
 */

const component = Vue.extend({
  name: 'ChannelCalibration',
  components: { ParamReadonly },
  filters: {
    /**
     * @param {number} value
     * @param {number} max
     * @return {number}
     */
    toFixed(value /*: number */, max /*: number */) {
      if (!Number.isFinite(value)) {
        return value;
      }
      return parseFloat(value.toFixed(max || 3));
    }
  },
  mixins: [ UrlUtilitiesMixin ],
  props: {
    card: { type: Object, default: null },
    channel: { type: Number, default: null },
    inEdit: { type: Boolean, default: false }
  },
  /**
   * @return {{
   *  dimService?: ?DicXmlDataSet,
   *  fullScale: ?number, offset: ?number, threshold: ?number
   * }}
   */
  data() {
    return mapValues(ChannelCalibrationMap, constant(null));
  },
  computed: {
    ...mapState({ dns: 'dns', daq: 'daqName' }),
    /**
     * @return {?number}
     */
    thresholdMV() {
      if (!this.threshold || !this.fullScale) { return null; }
      const bits = get(CardInfo, [ this.card.type, 'bits' ], 0);
      return this.threshold * this.fullScale / (1 << bits);
    }
  },
  mounted() { this.monitor(); },
  beforeDestroy() { this.close(); },
  methods: {
    /** @this {Instance} */
    close() {
      if (this.dimService) {
        this.dimService.close();
        this.dimService.removeAllListeners();
        this.dimService = null;
      }
    },
    /** @this {Instance} */
    monitor() {
      this.close();
      // @ts-ignore
      assign(this, this.$options.data()); /* reset state */
      this.dimService = new DicXmlDataSet(`${this.daq}/${this.card.name}/CHANNEL${this.channel}/Calibration`,
        { proxy: this.getProxy() }, this.dnsUrl(this.dns));

      this.dimService.promise()
      .catch((err) => logger.error(err));
      this.dimService.on('value', (value) => {
        /** @type {any[]} */
        var vals = transform(value, (ret, val) => {
          // @ts-ignore
          ret[val.index] = val;
        }, []);

        forEach(ChannelCalibrationMap, (param, name) => {
          // @ts-ignore
          this[name] = get(vals, [ param.idx, 'value' ], null);
        });

        debug('channel calibration update', this.getKey());
      });
    }
  }
});
export default component;
