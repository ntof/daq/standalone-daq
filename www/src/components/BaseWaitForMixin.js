// @ts-check
import q from 'q';
import Vue from 'vue';

/**
 * @typedef {V.Instance<typeof component>} Instance
 */

/**
 * @brief mixin method to wait for a value in promise chains
 */
const component = Vue.extend({
  methods: {
    /**
     * @this {Instance}
     * @param {string} data
     * @param {any} value
     * @param {number} timeout
    */
    waitFor(data, value, timeout) {
      const def = q.defer();
      const unwatch = this.$watch(data, (v) => {
        if (value === v) { def.resolve(); }
      }, { immediate: true });
      return def.promise.timeout(timeout || 5000)
      .finally(() => unwatch());
    }
  }
});
export default component;
