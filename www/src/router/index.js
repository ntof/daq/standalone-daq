// @ts-check
import { assign, get, hasIn } from 'lodash';
import Vue from 'vue';
import Router from 'vue-router';
import Dashboard from '../components/Dashboard.vue';
import { currentUrl } from '../utilities';
import store from '../store';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/', name: 'Dashboard', component: Dashboard
    },
    { path: '/index.html', redirect: '/' },
    { path: '/Dashboard', redirect: '/' }
  ],
  base: currentUrl()
});

// default route to ntofdaq-m12
router.beforeEach((to, from, next) => {
  if (!hasIn(to.query, 'dns') ||
      (get(to.query, 'dns') === 'ntofdaq-m12.cern.ch' && !hasIn(to.query, 'daq'))) {
    return next({ path: '/', query: assign({}, to.query,
      { dns: 'ntofdaq-m12.cern.ch', daq: 'ntofdaq-m12' }) });
  }
  else {
    return next();
  }
});

// eslint-disable-next-line @typescript-eslint/no-unused-vars
router.afterEach((to, from) => { /* jshint unused:false */
  store.commit('queryChange', to.query);
});

export default router;
