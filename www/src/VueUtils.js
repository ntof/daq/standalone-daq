// @ts-check
import _ from 'lodash';
import './public-path';

const utils = {
  /**
   * @param  {any} value
   * @return {boolean}
   */
  isEmpty: function(value) {
    return _.isEmpty(value);
  },

  /**
   * @param  {string|number} timestamp
   * @return {string}
   */
  timestampToTime: function(timestamp) {
    const date = new Date(timestamp);
    const hours = date.getHours();
    const minutes = '0' + date.getMinutes();
    const seconds = '0' + date.getSeconds();
    return hours + ':' + minutes.substr(-2) + ':' + seconds.substr(-2);
  },

  /**
   * @param  {string} url
   * @return {string}
   */
  publicPath: function(url) {
    /* eslint-disable-next-line camelcase */ /* @ts-ignore */
    if (typeof __webpack_public_path__ !== 'undefined') {
      /* eslint-disable-next-line camelcase */ /* @ts-ignore */
      return __webpack_public_path__ + url;
    }
    return url;
  }
};

export default {
  /** @param {InstanceType<Vue>} Vue */
  install(Vue) {
    Vue.helpers = utils;
    Vue.prototype.$utils = utils;
  }
};
