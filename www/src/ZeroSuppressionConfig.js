// @ts-check
/* eslint-disable max-lines */
import _ from 'lodash';
import d from 'debug';
import { ZSPChannelMap, ZSPMap } from './interfaces/daq';

var debug = d('daq:zsp');

/**
 * @typedef {string} ZSPId
 * @typedef {{
 *  id: ZSPId,
 *  sn: string,
 *  channel: number
 * }} Channel
 * @typedef {Channel & { masterId?: ZSPId }} Slave
 * @typedef {Channel & { slave: Slave[] }} Master
 */

/*::
import type { $Element } from 'xmldom'

export type ZSP$Id = string

export type ZSP$Channel = {
  id: ZSP$Id,
  sn: string,
  channel: number
}
export type ZSP$Slave = ZSP$Channel & { masterId?: ZSP$Id }

export type ZSP$Master = ZSP$Channel & { slave: Array<ZSP$Slave> }

*/

/**
 * @template T
 * @param {Partial<Channel&Slave&Master> & { sn:string, channel: number }} chan
 * @return {T}
 */
function genId(chan) {
  _.set(chan, 'id', chan.sn + ':' + chan.channel);
  // @ts-ignore
  return chan;
}

/**
 * @param {redim.XmlData.Data} data
 * @return {Channel|Master|Slave}
 */
function parseZspChannel(data) {
  /** @type {Partial<Channel&Master&Slave>} */
  var ret = _.transform(data.value, (/** @type {Partial<Channel&Master&Slave>} */ ret, d) => {
    switch (d.index) {
    case ZSPChannelMap.channel.idx:
      ret.channel = _.toNumber(d.value);
      break;
    case ZSPChannelMap.sn.idx:
      ret.sn = d.value;
      break;
    default:
      if (!ret.slave) { ret.slave = []; }
      ret.slave.push(parseZspChannel(d));
      break;
    }
  }, {});

  // @ts-ignore
  ret = genId(ret);
  if (ret.slave) {
    ret.slave = _.orderBy(ret.slave, [ 'sn', 'channel' ]);
    _.forEach(ret.slave, (s) => (s.masterId = ret.id));
  }
  return /** @type {Channel|Master|Slave} */ (ret);
}

/**
 * @param {Master|Slave|Channel} info
 * @param {number} index
 * @return {redim.XmlData.NestedData}
 */
function makeZspChannel(info /*: ZSP$Master */, index /*: number */) {
  /** @type {redim.XmlData.NestedData} */
  const ret = {
    index,
    value: [
      { index: ZSPChannelMap.sn.idx, type: ZSPChannelMap.sn.type, value: info.sn },
      { index: ZSPChannelMap.channel.idx, type: ZSPChannelMap.channel.type, value: info.channel }
    ]
  };
  _.forEach(/** @type {Master} */ (info).slave,
    (s, i) => ret.value.push(makeZspChannel(s, i + 2)));
  return ret;
}

/**
 * @param {Master|Slave|Channel} zsp1
 * @param {Master|Slave|Channel} zsp2
 * @return {boolean}
 */
function zspCmp(zsp1 /*: ZSP$Channel */, zsp2 /*: ZSP$Channel */) {
  if (zsp1.sn === zsp2.sn) {
    return zsp1.channel < zsp2.channel;
  }
  return (zsp1.sn < zsp2.sn);
}

/** @enum {number} */
const modes = {
  INDEPENDENT: 0,
  SINGLE_MASTER: 1,
  MASTER: 2
};

export class ZSPConfig {
  /**
   * @param {modes} mode
   */
  constructor(mode /*: number */) {
    this.mode = mode;
    /** @type {Master[]} */
    this.master = [];
    /** @type {Slave[]} */
    this.indep = [];
  }

  /**
   * @param {ZSPId} masterId
   * @param {boolean} promote
   * @return {Master|undefined}
   */
  _findMaster(masterId /*: ?ZSP$Id */, promote /*: boolean */) /* ?ZSP$Master */ {
    var ret = _.find(this.master, { id: masterId });
    if (!ret && promote) {
      ret = /** @type {Master} */ (_.find(this.indep, { id: masterId }));
      if (ret) {
        this.indep = _.without(this.indep, ret);
        ret.slave = [];
        this.master = /** @type {Master[]} */ (this._insert(this.master, ret));
      }
    }
    return ret;
  }

  /**
   * @param {Master} master
   */
  _checkMaster(master /*: ZSP$Master */) {
    if (_.isEmpty(master.slave)) {
      this.master = _.without(this.master, master);
      _.unset(master, 'slave');
      this.indep = this._insert(this.indep, master);
    }
  }

  /**
   * @param {(Master|Slave)[]} master
   * @param {any} slave
   */
  _insert(master /*: Array<any> */, slave /*: any */) {
    var ret = _.cloneDeep(master);
    var idx = _.findIndex(ret, _.partial(zspCmp, slave));
    ret.splice((idx >= 0) ? idx : ret.length, 0, slave);
    return ret;
  }

  /**
   * @param {modes} mode
   */
  setMode(mode) {
    switch (mode) {
    case ZSPConfig.MODE.SINGLE_MASTER:
      if (!_.isEmpty(this.master)) {
        // @ts-ignore checked above
        this.setSingleMaster(_.first(this.master).id);
      }
      break;
    case ZSPConfig.MODE.MASTER:
      _.forEach(this.master, (m) => this._checkMaster(m));
      break;
    }
    this.mode = mode;
  }

  /**
   * @param {ZSPId} masterId
   */
  setSingleMaster(masterId /*: ZSP$Id */) {
    _.forEach(this.master, (master) => {
      _.forEach(master.slave, (slave) => {
        this.indep = this._insert(this.indep, slave);
        _.unset(slave, 'masterId');
      });
      master.slave = [];
      this._checkMaster(master);
    });
    this._findMaster(masterId, true); /* used for promotion */
  }

  /**
   * @param {Slave} slave
   * @param {ZSPId} masterId
   */
  moveSlave(slave /*: ZSP$Slave */, masterId /*: ?ZSP$Id */) {
    if (!slave || slave.masterId === masterId || slave.id === masterId) {
      debug('not moving', slave);
      return;
    }
    if (slave.masterId) {
      const master = this._findMaster(slave.masterId, false);
      if (!master) {
        debug('master not found', slave.masterId);
      }
      else {
        debug('removing from master', master, slave);
        master.slave = _.without(master.slave, slave);
        this._checkMaster(master);
      }
    }
    else {
      debug('removing from indep', slave);
      this.indep = _.without(this.indep, slave);
    }

    if (!_.isEmpty(masterId)) {
      const master = this._findMaster(masterId, true);
      if (!master) {
        debug('master not found', masterId);
      }
      else {
        debug('adding to master', master, slave);
        master.slave = this._insert(master.slave, slave);
      }
      /* $FlowIgnore */
      slave.masterId = masterId;
    }
    else {
      debug('adding to indep', slave);
      this.indep = this._insert(this.indep, slave);
      _.unset(slave, 'masterId');
    }
  }

  /**
   *
   * @param {redim.XmlData.Data[]} params
   * @return {ZSPConfig?}
   */
  static fromParam(params /*: Array<DIMData> */) /*: ?ZSPConfig */ {
    debug('loading from params', params);
    if (!params) {
      return null;
    }
    const mode = _.get(_.find(params, { index: ZSPMap.mode.idx }), 'value');
    if (_.isNil(mode)) {
      debug('mode not found in ZSP');
      return null;
    }

    const zsp = new ZSPConfig(mode);
    const config = _.get(_.find(params, { index: ZSPMap.configuration.idx }), 'value');
    zsp.master = /** @type {Master[]} */ (_.orderBy(_.map(config, parseZspChannel), [ 'sn', 'channel' ]));
    return zsp;
  }

  /** @return {redim.XmlData.Data[]} */
  toParams() {
    return [
      { index: ZSPMap.mode.idx, type: ZSPMap.mode.type, value: this.mode },
      { index: ZSPMap.configuration.idx,
        value: _.map(this.master, (m /*: ZSP$Master */, i) => makeZspChannel(m, i)) }
    ];
  }

  /**
   * @param {{ [id: string]: any }} channels
   */
  updateIndep(channels /*: { [ZSP$Id]: any } */) {
    var ret = _.mapValues(channels, () => null);
    _.forEach(this.master, (master) => {
      _.unset(ret, master.id);
      _.forEach(master.slave, (slave) => {
        _.unset(ret, slave.id);
      });
    });

    /** @type {Slave[]} */
    const indeps = _.map(ret, (nop, id) => {
      var chan = channels[id];
      return genId({
        sn: _.get(chan, 'card.serialNumber'),
        channel: chan.channel
      });
    });
    this.indep = _.orderBy(indeps, [ 'sn', 'channel' ]);
  }

  clone() /*: ZSPConfig */ {
    var ret = new ZSPConfig(this.mode);
    ret.master = _.cloneDeep(this.master);
    ret.indep = _.cloneDeep(this.indep);
    return ret;
  }
}

ZSPConfig.MODE = modes;
