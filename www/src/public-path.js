
/* for webpack module loaders */
import { currentUrl } from './utilities';

// eslint-disable-next-line
if (typeof __webpack_public_path__ !== 'undefined' &&
  !('__karma__' in window)) {
  // eslint-disable-next-line
  __webpack_public_path__ = currentUrl() + '/dist/'; // jshint ignore:line
}
