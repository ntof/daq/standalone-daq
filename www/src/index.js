// @ts-check
import './public-path';

import Vue from 'vue';
import store from './store';
import VueRouter from 'vue-router';
// @ts-ignore
import App from './App';
import router from './router';
import '../static/css/style.css';
import BaseVue from '@cern/base-vue';
import NtofVueWidgets from '@ntof/ntof-vue-widgets';

import VueUtils from './VueUtils';

Vue.config.productionTip = false;

Vue.use(VueUtils);
Vue.use(VueRouter);
Vue.use(BaseVue, { auth: true });
Vue.use(NtofVueWidgets);

export default new Vue({
  el: '#dashboard',
  store,
  router,
  render: (h) => h(App)
});
