// @ts-check

import moment from 'moment';
import { has, isString, lowerFirst } from 'lodash';
import Vue from 'vue';

/** @type {string} */
var _currentUrl = window.location.origin + window.location.pathname;

/**
 * @return {string}
 */
export function currentUrl() {
  return _currentUrl;
}

/**
 * @param {string} url
 */
export function setCurrentUrl(url) {
  _currentUrl = url;
}

/**
 * @param  {string} dns DIM dns hostname
 * @return {string} dns proxy url
 */
export function dnsUrl(dns/*: any */) {
  return _currentUrl + '/' + dns + '/dns/query';
}

let id = 0;
/**
* @return {string}
*/
export function genId() {
  return 'a-' + (++id);
}


export const UrlUtilitiesMixin = Vue.extend({
  methods: {
    getProxy() {
      return _currentUrl;
    },
    currentUrl() {
      return _currentUrl;
    },
    /**
     * @param  {string} dns
     */
    dnsUrl(dns/*: any */) {
      return _currentUrl + '/' + dns + '/dns/query';
    }
  }
});

/**
 * @param  {?number=} timestamp
 * @return {string}
 */
export const getHumanReadableDate = (timestamp /*: number*/) => {
  if (!timestamp || timestamp === 0) {
    return "Not set.";
  }
  var x = moment.unix(timestamp);
  var y = moment();
  // if (x < y) { return "Expired."; } // Should never happen?
  return moment.duration(x.diff(y)).humanize(true);
};

/**
 * @param  {string} prefix string to append on errors
 * @param  {string|Error} err
 * @return {string|Error}
 */
export function errorPrefix(prefix /*: string*/, err /*: any */) {
  if (isString(err)) {
    return prefix + err;
  }
  else if (has(err, 'message')) {
    err.message = prefix + lowerFirst(err.message);
  }
  throw err;
}

/**
 * @brief replaces Q.Promise.tap
 * @template T=any
 * @param  {(ret: T) => any} fun
 * @return {(ret: T) => Promise<T>}
 */
export function tap(fun) {
  return function(ret) {
    return Promise.resolve(fun(ret))
    .then(() => ret);
  };
}
