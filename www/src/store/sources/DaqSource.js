// @ts-check
import d from 'debug';
import { bindAll, get } from 'lodash';
import { DicXmlState } from '@ntof/redim-client';
import { currentUrl, dnsUrl } from '../../utilities';

import DIMSource from './DIMSource';

const debug = d('app:store:daq');

/**
 * @typedef {{ code: number, message: string }} ErrWarn
 */

export default class DaqSource extends DIMSource {
  /**
   * @param {V.Store<AppStore.State>} store
   */
  constructor(store /*: App$Store */) {
    super(store, 'daqName');
    this.services = [ 'daqState', 'acqState' ];

    /** @type {DicXmlState|null} */
    this.daqState = null;
    /** @type {DicXmlState|null} */
    this.acqState = null;

    bindAll(this, [ 'onError', 'onDaqState', 'onAcqState' ]);
    this.connect();
  }

  /**
   * @param {?string} daq
   */
  onConnect(daq) {
    debug('connecting to DAQ:', daq);
    this.close();
    const dns = get(this.store, [ 'state', 'dns' ]);
    if (dns && daq) {
      this.daqState = new DicXmlState(`${daq}/DaqState`,
        { proxy: currentUrl() }, dnsUrl(dns));
      this.daqState.on('error', this.onError);
      this.daqState.on('value', this.onDaqState);
      this.daqState.promise().catch(this.onError);

      this.acqState = new DicXmlState(`${daq}/AcquisitionState`,
        { proxy: currentUrl() }, dnsUrl(dns));
      this.acqState.on('error', this.onError);
      this.acqState.on('value', this.onAcqState);
      this.acqState.promise().catch(this.onError);
    }
    else {
      this.onDaqState(null);
      this.onAcqState(null);
    }
  }

  /**
   * @param  {redim.XmlState.State|null} value
   */
  onDaqState(value) {
    debug('daqState:', value);
    this.store.commit('daq/daqState', value);
  }

  /**
   * @param  {redim.XmlState.State|null} value
   */
  onAcqState(value) {
    debug('acqState:', value);
    this.store.commit('daq/acqState', get(value, 'value'));
  }
}
