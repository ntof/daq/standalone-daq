// @ts-check
import { forEach, get } from 'lodash';
import { BaseLogger as logger } from '@cern/base-vue';

/**
 * @brief helper abstract class for DIM based sources
 */
export default class DIMSource {
  /**
   * @param {V.Store<AppStore.State>} store
   * @param {string|string[]} path
   */
  constructor(store, path) {
    this.store = store;
    this.storePath = path;
    this.unwatch = this.store.watch(
      (state) => get(state, this.storePath),
      (value) => this.onConnect(value));
    /** @type {string[]|null} */
    this.services = null;
  }

  connect() {
    this.onConnect(get(this.store.state, this.storePath));
  }

  destroy() {
    this.close();
    this.unwatch();
  }

  /**
   * @param {?string} value
   */
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  onConnect(value) { /* jshint unused:false */
    this.close();
  }

  close() {
    // @ts-ignore this stuff should be down in the hierarchy
    if (this[get(this, [ 'services', 0 ])]) {
      // @ts-ignore
      forEach(this.services, (srv) => {
        // @ts-ignore
        this[srv].close();
        // @ts-ignore
        this[srv] = null;
      });
    }
  }

  /**
   * @param {any} err
   */
  onError(err /*: any */) {
    logger.error(err);
  }
}
