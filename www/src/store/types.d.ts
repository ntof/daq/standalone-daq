
import { DaqState as DaqStateEnum, AcqState as AcqStateEnum } from '../interfaces/daq'

export = AppStore
export as namespace AppStore

declare namespace AppStore {
  interface StateErrWarn {
    code: number,
    message: string
  }

  interface StateValue {
    strValue: string,
    value: number,
    errors: Array<ErrWarn>,
    warnings: Array<ErrWarn>
  }

  interface DaqState {
    name: string|null;
    daqState: DaqStateEnum|null;
    acqState: AcqStateEnum|null;
    daqStateValue: StateValue|null;
  }

  interface State {
    dns: string|null,
    daqName: string|null,
    user: { username: string, [key: string]: any } | null
  }
}
