// @ts-check

import { get } from 'lodash';

/** @type {V.Module<AppStore.DaqState>} */
const module = {
  namespaced: true,
  state: () => ({
    name: null,
    daqState: null,
    acqState: null,
    daqStateValue: null
  }),
  mutations: {
    daqState(store, value) {
      store.daqState = get(value, 'value', null);
      store.daqStateValue = value;
    },
    acqState(store, value) { store.acqState = value; }
  }
};
export default module;
