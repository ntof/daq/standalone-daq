// @ts-check

import { assign, get, merge } from 'lodash';
import Vuex from 'vuex';
import Vue from 'vue';
import daq from './modules/daq';
import DaqSource from './sources/DaqSource';
import {
  sources as baseSources,
  createStore,
  storeOptions } from '@cern/base-vue';

Vue.use(Vuex);

/** @type {V.Store<AppStore.State>} */
merge(storeOptions, /** @type {V.StoreOptions<AppStore.State>} */ ({
  state: {
    dns: null,
    daqName: null,
    user: null
  },
  mutations: {
    queryChange(state, query) {
      state.dns = get(query, 'dns', null);
      state.daqName = get(query, 'daq', null);
    },
    clear(state) {
      assign(state, { dns: null, daqName: null, user: null });
    }
  },
  modules: {
    daq: daq
  }
}));

const store = /** @type {V.Store<AppStore.State>} */ (createStore());
export default store;

export const sources = merge(baseSources, {
  daq: new DaqSource(store)
});
