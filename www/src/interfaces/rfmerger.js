// @ts-check

import { ParamType } from "./index";

/** @enum {number} */
export const RFMergerState = {
  OK: 0
};

export class RFMRunFile {
  /**
   * @param {number} runNumber
   * @param {number} index
   */
  constructor(runNumber /*: number */, index /*: number */) {
    this.runNumber = runNumber;
    this.index = index;
    this.name = "";
    this.status = "";
    this.ignored = false;
    this.size = 0; // Bytes
    this.transferred = 0;
    this.rate = 0;
    this.progress = 0;
  }
}

export class RFMergerStats {
  constructor() {
    this.incomplete = 0;
    this.waitingApproval = 0;
    this.waiting = 0;
    this.transferring = 0;
    this.copied = 0;
    this.migrated = 0;
    this.failed = 0;
    this.ignored = 0;
    this.transferred = 0;
    this.rate = 0;
  }
}

export class RFMergerRun extends RFMergerStats {
  constructor() {
    super();
    this.runNumber = -1;
    this.experiment = '';
    /** @type {?boolean} */
    this.approved = null;
    /** @type {?number} */
    this.startDate = null;
    /** @type {?number} */
    this.stopDate = null;
    /** @type {?number} */
    this.expiryDate = null;
  }

}

export const RunFilesMap = {
  status: { idx: 0, type: ParamType.STRING },
  ignored: { idx: 1, type: ParamType.BOOL },
  size: { idx: 2, type: ParamType.UINT64 },
  transferred: { idx: 3, type: ParamType.UINT64 },
  rate: { idx: 4, type: ParamType.UINT64 }
};

export const RunFilesMapRpc = {
  status: { idx: 0, type: ParamType.STRING },
  ignored: { idx: 1, type: ParamType.BOOL },
  size: { idx: 2, type: ParamType.UINT64 },
  transferred: { idx: 3, type: ParamType.UINT64 }
};

export const RunParamsMap = {
  runNumber: { idx: 0, type: ParamType.UINT32 },
  experiment: { idx: 1, type: ParamType.STRING },
  approved: { idx: 2, type: ParamType.BOOL },
  startDate: { idx: 3, type: ParamType.UINT32 },
  stopDate: { idx: 4, type: ParamType.UINT32 },
  expiryDate: { idx: 5, type: ParamType.UINT32 },
  transferred: { idx: 6, type: ParamType.UINT64 },
  rate: { idx: 7, type: ParamType.UINT64 },
  incomplete: { idx: 8, type: ParamType.UINT32 },
  waitingApproval: { idx: 9, type: ParamType.UINT32 },
  waiting: { idx: 10, type: ParamType.UINT32 },
  transferring: { idx: 11, type: ParamType.UINT32 },
  copied: { idx: 12, type: ParamType.UINT32 },
  migrated: { idx: 13, type: ParamType.UINT32 },
  ignored: { idx: 14, type: ParamType.UINT32 },
  failed: { idx: 15, type: ParamType.UINT32 }
};

export const CurrentRunParamsMap = {
  runNumber: { idx: 1, type: ParamType.UINT32 },
  experiment: { idx: 2, type: ParamType.STRING },
  approved: { idx: 3, type: ParamType.BOOL },
  startDate: { idx: 4, type: ParamType.UINT32 },
  stopDate: { idx: 5, type: ParamType.UINT32 },
  expiryDate: { idx: 6, type: ParamType.UINT32 },
  transferred: { idx: 7, type: ParamType.UINT64 },
  rate: { idx: 8, type: ParamType.UINT64 },
  incomplete: { idx: 9, type: ParamType.UINT32 },
  waitingApproval: { idx: 10, type: ParamType.UINT32 },
  waiting: { idx: 11, type: ParamType.UINT32 },
  transferring: { idx: 12, type: ParamType.UINT32 },
  copied: { idx: 13, type: ParamType.UINT32 },
  migrated: { idx: 14, type: ParamType.UINT32 },
  ignored: { idx: 15, type: ParamType.UINT32 },
  failed: { idx: 16, type: ParamType.UINT32 }
};
