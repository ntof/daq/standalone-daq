// @ts-check

import { toNumber } from 'lodash';
import { DicXmlDataSet } from '@ntof/redim-client';

export const ParamType = DicXmlDataSet.DataType;

/**
 * @param {any} value
 * @param {redim.XmlData.DataType} type
 * @return {boolean|number|string}
 */ // eslint-disable-next-line complexity
export function parseValue(value, type) {
  switch (type) {
  case ParamType.INT64:
  case ParamType.INT32:
  case ParamType.INT16:
  case ParamType.INT8:
  case ParamType.DOUBLE:
  case ParamType.FLOAT:
  case ParamType.ENUM:
  case ParamType.UINT32:
  case ParamType.UINT64:
  case ParamType.UINT8:
  case ParamType.UINT16:
    return toNumber(value);
  case ParamType.BOOL:
    return toNumber(value) > 0;
  default:
    return value;
  }
}
