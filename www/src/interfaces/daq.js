// @ts-check
import { ParamType } from './index';

/** @enum {number} */
export const DaqState = {
  HARDWARE_INIT: 0,
  NOT_CONFIGURED: 1,
  CALIBRATING: 2,
  WAITING_FOR_COMMAND: 3,
  PROCESSING_COMMAND: 4,
  STOPPING_ACQ: 8,
  STARTING_ACQ: 9,
  RESETING: 10,
  INITIALIZING_ACQ: 11,
  WAITING_FOR_START_ACQ: 12,
  SUPER_USER: 13
};

/** @enum {number} */
export const AcqState = {
  IDLE: 0,
  INITIALIZATION: 1,
  ALLOCATING_MEMORY: 2,
  WAITING_TO_START: 3,
  ARMING_CARD_TRIGGERS: 4,
  WAITING_FOR_TRIGGER: 5,
  DUMPING_CARD_MEM_BUFFERS: 6,
  QUEUING_BUFFERS_FOR_WRITER: 7
};

export const ChannelParamsMap = {
  enabled: { idx: 1, type: ParamType.BOOL },
  detectorType: { idx: 2, type: ParamType.STRING },
  detectorId: { idx: 3, type: ParamType.UINT32 },
  chassisId: { idx: 7, type: ParamType.UINT8 },
  sampleRate: { idx: 9, type: ParamType.FLOAT },
  sampleSize: { idx: 10, type: ParamType.UINT32 },
  fullScale: { idx: 11, type: ParamType.FLOAT },
  delay: { idx: 12, type: ParamType.INT32 },
  threshold: { idx: 13, type: ParamType.FLOAT },
  thresholdSign: { idx: 14, type: ParamType.INT32 },
  zsStart: { idx: 15, type: ParamType.UINT32 },
  offset: { idx: 16, type: ParamType.FLOAT },
  preSamples: { idx: 17, type: ParamType.UINT32 },
  postSamples: { idx: 18, type: ParamType.UINT32 },
  impedance: { idx: 20, type: ParamType.FLOAT }
};

export const ChannelCalibrationMap = {
  fullScale: { idx: 0, type: ParamType.FLOAT },
  offset: { idx: 1, type: ParamType.FLOAT },
  threshold: { idx: 2, type: ParamType.FLOAT }
};

export const ZSPMap = {
  mode: { idx: 1, type: ParamType.ENUM },
  configuration: { idx: 2 }
};

export const ZSPChannelMap = {
  sn: { idx: 0, type: ParamType.STRING },
  channel: { idx: 1, type: ParamType.UINT32 }
};
