// @ts-check

/** @enum {number} */
export const ParamsIdx = {
  MODE: 1,
  TRIGGER_REPEAT: 2,
  TRIGGER_PERIOD: 3,
  TRIGGER_PAUSE: 4,
  EVENT_NUMBER: 5
};

/** @enum {number} */
export const TimingMode = {
  DISABLED: 0,
  AUTOMATIC: 1,
  CALIBRATION: 2
};
