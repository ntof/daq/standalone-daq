// @ts-check
const
  express = require('express'),
  serveStatic = require('express-static-gzip'),
  helmet = require('helmet'),
  path = require('path'),
  { get, attempt, noop } = require('lodash'),
  ews = require('express-ws'),
  { makeDeferred } = require('@cern/prom'),

  { DisProxy, DnsProxy } = require('@ntof/redim-dim'),
  logger = require('./httpLogger'),
  auth = require('./auth');

/**
 * @typedef {import('express').Request} Request
 * @typedef {import('express').Response} Response
 * @typedef {import('express').NextFunction} NextFunction
 */

class Server {
  /**
   * @param {AppServer.Config} config
   */
  constructor(config) {
    this.config = config;
    this.app = express();
    this._prom = this.prepare(config);
  }

  /**
   * @param {AppServer.Config} config
   */
  async prepare(config) {
    if (process.env.NODE_ENV === 'production') {
      this.app.use(helmet());
    }
    logger.register(this.app);
    if (!get(config, 'noWebSocket', false)) {
      ews(this.app);
    }
    this.router = express.Router();
    const dist = path.join(__dirname, '..', 'www', 'dist');
    this.router.use('/dist', serveStatic(dist, { enableBrotli: true }));

    /* everything after this point is authenticated */
    if (config.auth) {
      await auth.register(this.router, config);
      this.router.use(this.runAuth.bind(this, 'user'));
    }

    this.app.set('view engine', 'pug');
    this.app.set('views', path.join(__dirname, 'views'));

    this.router.get('/', (req, res) => res.render('index', config));

    DnsProxy.register(this.router);
    DisProxy.register(this.router);

    if (config.basePath) {
      this.router.get('/beta',
        (req, res) => res.redirect(path.join('/beta', config.basePath)));
    }

    this.app.use(config.basePath, this.router);

    // default route
    this.app.use(function(req, res, next) {
      next({ status: 404, message: 'Not Found' });
    });

    // error handler
    this.app.use(function(
      /** @type {any} */ err,
      /** @type {Request} */ req,
      /** @type {Response} */res,
      /** @type {NextFunction} */ next) { /* jshint unused:false */ // eslint-disable-line
      res.locals.message = err.message;
      res.locals.error = req.app.get('env') === 'development' ? err : {};
      res.locals.status = err.status || 500;
      res.status(err.status || 500);
      res.render('error', { baseUrl: config.basePath });
    });
  }

  close() {
    if (this.server) {
      this.server.close();
      this.server = null;
    }
  }

  /**
   * @param {string|string[]} role
   * @param {Request} req
   * @param {Response} res
   * @param {NextFunction} next
   */
  runAuth(role, req, res, next) {
    if (req.path.startsWith('/dist')) {
      return next();
    }
    return auth.roleCheck(role, req, res, next);
  }

  /**
   * @param {() => any} cb
   */
  async listen(cb) {
    await this._prom;
    const def = makeDeferred();
    /* we're called as a main, let's listen */
    var server = this.app.listen(this.config.port, () => {
      this.server = server;
      def.resolve(undefined);
      return attempt(cb || noop);
    });
    return def.promise;
  }

  address() {
    if (this.server) {
      return this.server.address();
    }
    return null;
  }
}

module.exports = Server;
