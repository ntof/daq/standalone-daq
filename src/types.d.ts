
export = AppServer
export as namespace AppServer

declare namespace AppServer {
    interface Auth {
        clientID: string,
        clientSecret: string,
        callbackURL: string,
        logoutURL: string
    }
    interface Config {
        port: number,
        basePath: string,
        noWebSocket?: boolean,
        auth?: Auth
    }
}
